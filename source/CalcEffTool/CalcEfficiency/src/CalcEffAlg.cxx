// Gaudi
#include "GaudiKernel/IAlgTool.h"

#include "GaudiKernel/ITHistSvc.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include <iostream>
#include <fstream>
#include <vector>

// xAOD 
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODTrigMuon/L2StandAloneMuon.h"
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigMuon/L2CombinedMuon.h"
#include "xAODTrigMuon/L2CombinedMuonContainer.h"
#include "xAODTrigger/MuonRoIContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "GoodRunsLists/IGoodRunsListSelectorTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigConfInterfaces/ITrigConfigTool.h"
#include "TrkParameters/TrackParameters.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
#include "TrkVertexFitterInterfaces/IVertexFitter.h"

#include "TrigConfHLTData/HLTTriggerElement.h"

#include "TrigConfHLTData/HLTUtils.h"
#include "TrigConfHLTData/HLTTriggerElement.h"


#include "CalcEfficiency/CalcEffAlg.h"
#include "TrigConfL1Data/TriggerThreshold.h"
#include "TrigConfL1Data/CTPConfig.h"
//#include "TrigT1Interfaces/RecMuonRoI.h"
//#include "TrigT1Interfaces/RecEmTauRoI.h"
//#include "TrigT1Interfaces/RecJetRoI.h"

#include "CalcEfficiency/MuonExtUtils.h"

// My class

using namespace SG;
using namespace std;
using namespace TrigCompositeUtils;


///THis function is constructor.
CalcEffAlg::CalcEffAlg(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator),
  m_grlTool( "GoodRunsListSelectionTool/MyGRLTool" ),
  m_extrapolator("Trk::Extrapolator/AtlasExtrapolator")
{
  ///(https://twiki.cern.ch/twiki/bin/view/Sandbox/WritingYourOwnAthenaAlgorithms)Notice the calls of the declareProperty("electron_Et_min_cut", m_electron_Et_min_cut = 20*GeV) in the constructor. This makes the C++ m_electron_Et_min_cut variable configurable from the Python job options. The first argument is a string containing the Python name of the variable. The second is the C++ variable with an optional equals-sign followed by a default value. Configuration from Python will be explained more later when we get to the job options.
  declareProperty( "message", m_message=2);
  declareProperty( "OutputFile", m_etname );
  declareProperty( "TapMethod", m_tapmethod );
  declareProperty( "Extrapolate", m_useExt );
  declareProperty( "GRL", m_useGRL );
  declareProperty( "DataType", m_dataType );
}

StatusCode CalcEffAlg::initialize() {
  ATH_MSG_INFO("initialize()");
  ATH_MSG_INFO("My message: " << m_message);
  m_isFirstEvent = true; 

  //==============================================================
  //==  GRL Tools
  //==============================================================
  ATH_CHECK( m_grlTool.retrieve() );



  //==============================================================
  //==  Trigger Tools
  //==============================================================

  //ATH_CHECK( m_HLTMenuKey.initialize() );
  //ATH_CHECK( m_L1MenuKey.initialize() );


  //SG::ReadHandle<TrigConf::HLTMenu>  hltMenuHandle = SG::makeHandle( m_HLTMenuKey );
  //ATH_CHECK( hltMenuHandle.isValid() );

  //SG::ReadHandle<TrigConf::L1Menu>  l1MenuHandle = SG::makeHandle( m_L1MenuKey );
  //ATH_CHECK( l1MenuHandle.isValid() );


  ////==============================================================
  ////==  Tools
  ////==============================================================
  ATH_CHECK(m_extrapolator.retrieve());
  //ATH_CHECK(m_vrtfitter.retrieve());



  ////==============================================================
  ////==  MuonExtrapolatorUtils Class
  ////==============================================================
  m_ext.initialize( m_extrapolator );



  ////==============================================================
  ////==  VrtFitterUtils Class
  ////==============================================================
  //m_vft.initialize( m_vrtfitter );



  //==============================================================
  //==  EventTree Class
  //==============================================================



  //==============================================================
  //==  TagAndProbe Class
  //
  ///Check TagAndProbe.h about declare of addMesChain.
  ///int addMesChain( const string& name, const L1Item& mesL1, const string& mesHLT, const string& mesAddTag); 
  ///int initialize( const int& message,const bool& useExt,const std::string method,MuonExtUtils ext,VrtFitUtils vft,ToolHandle<Trig::TrigDecisionTool> tdt,const std::string dataType ); //!
  //==============================================================
  //m_tap.initialize( 0, m_useExt, m_tapmethod, m_ext, m_vft, m_dataType, m_extrapolator );
  //m_tap.addMesChain( "mu4", TagAndProbe::L1_MU4, "HLT_mu4", "none" );
  //m_tap.addMesChain( "mu6", TagAndProbe::L1_MU6, "HLT_mu6", "none" );
  //m_tap.addMesChain( "mu6ms", TagAndProbe::L1_MU6, "HLT_mu6_msonly", "none" );
  //m_tap.addMesChain( "mu10", TagAndProbe::L1_MU10, "HLT_mu10", "none" );
  //m_tap.addMesChain( "mu10nc", TagAndProbe::L1_MU10, "HLT_mu10_nomucomb", "none" );
  //m_tap.addMesChain( "mu11", TagAndProbe::L1_MU11, "HLT_mu11", "none" );
  //m_tap.addMesChain( "mu11nc", TagAndProbe::L1_MU11, "HLT_mu11_nomucomb", "none" );
  //m_tap.addMesChain( "mu14", TagAndProbe::L1_MU10, "HLT_mu14", "none" );
  //m_tap.addMesChain( "mu18", TagAndProbe::L1_MU15, "HLT_mu18", "none" );
  //m_tap.addMesChain( "mu20", TagAndProbe::L1_MU20, "HLT_mu20", "none" );
  //m_tap.addMesChain( "mu20il", TagAndProbe::L1_MU15, "HLT_mu20_iloose_L1MU15", "none" );
  //m_tap.addMesChain( "mu20ms", TagAndProbe::L1_MU20, "HLT_mu20_msonly", "none" );
  ////m_tap.addMesChain( "mu24il", TagAndProbe::L1_MU15, "HLT_mu24_iloose_L1MU15", "none" );
  ////m_tap.addMesChain( "mu24im", TagAndProbe::L1_MU20, "HLT_mu24_imedium", "none" );
  //m_tap.addMesChain( "mu26im", TagAndProbe::L1_MU20, "HLT_mu26_imedium", "none" );
  //m_tap.addMesChain( "mu26ivm", TagAndProbe::L1_MU20, "HLT_mu26_ivarmedium", "none" );
  //m_tap.addMesChain( "mu50", TagAndProbe::L1_MU20, "HLT_mu50", "none" );
  //m_tap.addMesChain( "mu60", TagAndProbe::L1_MU20, "HLT_mu60_0eta105_msonly", "none" );
  ////m_tap.addMesChain( "mu4FS", TagAndProbe::NOTHING, "HLT_mu20_mu8noL1", "HLT_mu20_mu8noL1" );
  //m_tap.addMesChain( "mu4FS", TagAndProbe::NOTHING, "HLT_mu4noL1", "none" );
  //m_tap.addMesChain( "mu4FSMU6", TagAndProbe::L1_MU6, "HLT_mu20_mu8noL1", "HLT_mu20_mu8noL1" );
  ////m_tap.addMesChain( "mu6FS", TagAndProbe::NOTHING, "HLT_mu20_mu8noL1", "HLT_mu20_mu8noL1" );
  //m_tap.addMesChain( "mu6FS", TagAndProbe::NOTHING, "HLT_mu4noL1", "none" );
  //m_tap.addMesChain( "mu6FSMU6", TagAndProbe::L1_MU6, "HLT_mu20_mu8noL1", "HLT_mu20_mu8noL1" );
  ////m_tap.addMesChain( "mu8FS", TagAndProbe::NOTHING, "HLT_mu20_mu8noL1", "HLT_mu20_mu8noL1" );
  //m_tap.addMesChain( "mu8FS", TagAndProbe::NOTHING, "HLT_mu4noL1", "none" );
  ////m_tap.addMesChain( "2mu4", TagAndProbe::L1_MU4, "HLT_2mu4", "HLT_mu4" );
  //m_tap.addMesChain( "2mu6", TagAndProbe::L1_MU6, "HLT_2mu6", "HLT_mu6" );
  //m_tap.addMesChain( "2mu10", TagAndProbe::L1_MU10, "HLT_2mu10", "HLT_mu10" );
  //m_tap.addMesChain( "2mu10nocb", TagAndProbe::L1_MU10, "HLT_2mu10_nomucomb", "HLT_mu10" );
  //m_tap.addMesChain( "mu11mu6", TagAndProbe::NOTHING, "HLT_mu11_nomucomb_mu6noL1_nscan03_L1MU11_2MU6_bTau", "HLT_mu11");

  // Trigger rate hists
  StatusCode sc = service("THistSvc", m_thistSvc);
  if(sc.isFailure()) {
    ATH_MSG_FATAL("Unable to retrieve THistSvc");
    return sc;
  } else {
    ATH_MSG_INFO("able to retrieve THistSvc");
  }

  //m_trigL1.push_back(TagAndProbe::L1_MU4);
  //m_trigEvent.push_back("HLT_noalg_L1MU4");
  //m_trigHLT.push_back("HLT_mu4");
  //m_trigThreshold.push_back( 4.0 );

  //m_trigL1.push_back(TagAndProbe::L1_MU6);
  //m_trigEvent.push_back("HLT_noalg_L1MU6");
  //m_trigHLT.push_back("HLT_mu6");
  //m_trigThreshold.push_back( 6.0 );

  //m_trigL1.push_back(TagAndProbe::L1_MU15);
  //m_trigEvent.push_back("HLT_noalg_L1MU20");
  //m_trigHLT.push_back("HLT_mu26_ivarmedium");
  //m_trigThreshold.push_back( 26.0 );

  //m_trigL1.push_back(TagAndProbe::L1_MU15);
  //m_trigEvent.push_back("HLT_noalg_L1MU20");
  //m_trigHLT.push_back("HLT_mu50");
  //m_trigThreshold.push_back( 50.0 );

  //m_trigL1.push_back(TagAndProbe::L1_MU15);
  //m_trigEvent.push_back("HLT_mu50");
  //m_trigHLT.push_back("HLT_mu50");
  //m_trigThreshold.push_back( 50.0 );

  //m_trigL1.push_back(TagAndProbe::L1_MU4);
  //m_trigEvent.push_back("HLT_mu4");
  //m_trigHLT.push_back("HLT_mu4");
  //m_trigThreshold.push_back( 4.0 );

  //m_trigL1.push_back(TagAndProbe::L1_MU4);
  //m_trigEvent.push_back("HLT_noalg_L1MU4");
  //m_trigHLT.push_back("HLT_noalg_L1MU4");
  //m_trigThreshold.push_back( 4.0 );


  TString etname = m_etname.c_str();
  m_file 	= new TFile( etname, "recreate" );
  ATH_MSG_INFO("OutputFile: " << etname);

  const int NChain = m_trigEvent.size();

  for ( int i = 0; i< NChain; i++){
    m_h_countEvent.push_back(new TH1D(Form("countEvent_%s_%s",       m_trigEvent[i].data(), m_trigHLT[i].data()), "countEvent;Run number;Counts",    100, -3000000000,  3000000000));
    m_h_countL1.push_back(new TH1F(Form("countL1_%s_%s",             m_trigEvent[i].data(), m_trigHLT[i].data()), "countL1;#eta_{RoI};Counts",       100, -3, 3));
    m_h_countSA.push_back(new TH1F(Form("countSA_%s_%s",             m_trigEvent[i].data(), m_trigHLT[i].data()), "countSA;#eta_{RoI};Counts",       100, -3, 3));
    m_h_countCB.push_back(new TH1F(Form("countCB_%s_%s",             m_trigEvent[i].data(), m_trigHLT[i].data()), "countCB;#eta_{RoI};Counts",       100, -3, 3));
    m_h_countEF.push_back(new TH1F(Form("countEF_%s_%s",             m_trigEvent[i].data(), m_trigHLT[i].data()), "countEF;#eta_{RoI};Counts",       100, -3, 3));
    m_h_countOff.push_back(new TH1F(Form("countOff_%s_%s",           m_trigEvent[i].data(), m_trigHLT[i].data()), "countOff;#eta_{RoI};Counts",      100, -3, 3));
    m_h_countOffPtCut.push_back(new TH1F(Form("countOffPtCut_%s_%s", m_trigEvent[i].data(), m_trigHLT[i].data()), "countOffPtCut;#eta_{RoI};Counts", 100, -3, 3));
  }


  const int nbin_dR = 50;
  const double xmin_dR = 0;
  const double xmax_dR = 1;

  m_h_pt_dR_L1_0   = new TH2F("m_h_pt_dR_L1_0",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SA_0   = new TH2F("m_h_pt_dR_SA_0",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CB_0   = new TH2F("m_h_pt_dR_CB_0",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SAIO_0 = new TH2F("m_h_pt_dR_SAIO_0", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CBIO_0 = new TH2F("m_h_pt_dR_CBIO_0", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);

  m_h_pt_dR_L1_1   = new TH2F("m_h_pt_dR_L1_1",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SA_1   = new TH2F("m_h_pt_dR_SA_1",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CB_1   = new TH2F("m_h_pt_dR_CB_1",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SAIO_1 = new TH2F("m_h_pt_dR_SAIO_1", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CBIO_1 = new TH2F("m_h_pt_dR_CBIO_1", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);

  m_h_pt_dR_L1_2   = new TH2F("m_h_pt_dR_L1_2",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SA_2   = new TH2F("m_h_pt_dR_SA_2",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CB_2   = new TH2F("m_h_pt_dR_CB_2",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SAIO_2 = new TH2F("m_h_pt_dR_SAIO_2", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CBIO_2 = new TH2F("m_h_pt_dR_CBIO_2", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);

  m_h_pt_dR_L1_3   = new TH2F("m_h_pt_dR_L1_3",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SA_3   = new TH2F("m_h_pt_dR_SA_3",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CB_3   = new TH2F("m_h_pt_dR_CB_3",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SAIO_3 = new TH2F("m_h_pt_dR_SAIO_3", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CBIO_3 = new TH2F("m_h_pt_dR_CBIO_3", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);

  m_h_pt_dR_L1_4   = new TH2F("m_h_pt_dR_L1_4",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SA_4   = new TH2F("m_h_pt_dR_SA_4",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CB_4   = new TH2F("m_h_pt_dR_CB_4",   "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_SAIO_4 = new TH2F("m_h_pt_dR_SAIO_4", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);
  m_h_pt_dR_CBIO_4 = new TH2F("m_h_pt_dR_CBIO_4", "", 100, 0, 50, nbin_dR, xmin_dR, xmax_dR);



  m_h_dimumass = new TH1F("m_h_dimumass",     ";Mass [GeV];Counts", 300, 1, 5);
  m_h_l2cbio_dimuMass = new TH1F("m_h_l2cbio_dimuMass",     ";Mass [GeV];Counts", 300, 1, 5);
  m_h_di_ext_deltaR = new TH1F("m_h_di_ext_deltaR",     ";#{Delta}R;Counts", 100, 0, 4);
  m_h_di_IP_deltaR = new TH1F("m_h_di_IP_deltaR",     ";#{Delta}R;Counts", 100, 0, 4);

  m_h_di_dR_pt = new TH2F("m_h_di_dR_pt",     ";#Delta R;p_{T}", 300, 0, 0.6, 300, 0, 400);
  m_h_di_ext_IP_deltaR = new TH2F("m_h_di_extIP_deltaR",     "#{Delta}R;#Delta R;#Delta R;Counts", 400, 0, 1, 400, 0, 1);


  m_h_pt_alpha = new TH2F("m_h_pt_alpha",   ";", 400, 0, 0.25, 400, 0, 0.4);
  m_h_pt_beta  = new TH2F("m_h_pt_beta",    ";", 400, 0, 0.25, 400, 0, 0.4);
  m_h_pt_alpha1 = new TH2F("m_h_pt_alpha1", ";", 400, 0, 0.25, 400, 0, 0.4);
  m_h_pt_beta1  = new TH2F("m_h_pt_beta1",  ";", 400, 0, 0.25, 400, 0, 0.4);

  m_h_pt_muL1     = new TH1F("m_h_pt_muL1",     ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muSA     = new TH1F("m_h_pt_muSA",     ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muCB     = new TH1F("m_h_pt_muCB",     ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muSAIO   = new TH1F("m_h_pt_muSAIO",   ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muCBIO   = new TH1F("m_h_pt_muCBIO",   ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muL1_1   = new TH1F("m_h_pt_muL1_1",   ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muSA_1   = new TH1F("m_h_pt_muSA_1",   ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muCB_1   = new TH1F("m_h_pt_muCB_1",   ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muSAIO_1 = new TH1F("m_h_pt_muSAIO_1", ";p_{T}[GeV];Counts", 40, 0, 20);
  m_h_pt_muCBIO_1 = new TH1F("m_h_pt_muCBIO_1", ";p_{T}[GeV];Counts", 40, 0, 20);

  m_h_ftf_size     = new TH2F("m_h_ftf_size",   "", 40, 0, 50, 100, -2, 15);
  m_h_ftf_size1     = new TH2F("m_h_ftf_size1", "", 40, 0, 50, 100, -2, 15);
  m_h_ftf_size2     = new TH2F("m_h_ftf_size2", "", 40, 0, 50, 100, -2, 15);


  // pt residual
  m_h_pt_res_muL1     = new TH2F("m_h_pt_res_muL1",     ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muSA     = new TH2F("m_h_pt_res_muSA",     ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muCB     = new TH2F("m_h_pt_res_muCB",     ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muSAIO   = new TH2F("m_h_pt_res_muSAIO",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muCBIO   = new TH2F("m_h_pt_res_muCBIO",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);

  m_h_pt_res_muL1_1   = new TH2F("m_h_pt_res_muL1_1",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muSA_1   = new TH2F("m_h_pt_res_muSA_1",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muCB_1   = new TH2F("m_h_pt_res_muCB_1",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muSAIO_1 = new TH2F("m_h_pt_res_muSAIO_1", ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muCBIO_1 = new TH2F("m_h_pt_res_muCBIO_1", ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);

  m_h_pt_res_muL1_2   = new TH2F("m_h_pt_res_muL1_2",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muSA_2   = new TH2F("m_h_pt_res_muSA_2",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muCB_2   = new TH2F("m_h_pt_res_muCB_2",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muSAIO_2 = new TH2F("m_h_pt_res_muSAIO_2", ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muCBIO_2 = new TH2F("m_h_pt_res_muCBIO_2", ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);

  m_h_pt_res_muL1_3   = new TH2F("m_h_pt_res_muL1_3",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muSA_3   = new TH2F("m_h_pt_res_muSA_3",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muCB_3   = new TH2F("m_h_pt_res_muCB_3",   ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muSAIO_3 = new TH2F("m_h_pt_res_muSAIO_3", ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);
  m_h_pt_res_muCBIO_3 = new TH2F("m_h_pt_res_muCBIO_3", ";p_{T}[GeV];Residual", 40, 0, 20, 500, -5, 5);


  // residual between Road and SuperPoint
  m_h_pt_resSeg_muL1     = new TH2F("m_h_pt_resSeg_muL1",     ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muSA     = new TH2F("m_h_pt_resSeg_muSA",     ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muCB     = new TH2F("m_h_pt_resSeg_muCB",     ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muSAIO   = new TH2F("m_h_pt_resSeg_muSAIO",   ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muCBIO   = new TH2F("m_h_pt_resSeg_muCBIO",   ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);

  m_h_pt_resSeg_muL1_1   = new TH2F("m_h_pt_resSeg_muL1_1",   ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muSA_1   = new TH2F("m_h_pt_resSeg_muSA_1",   ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muCB_1   = new TH2F("m_h_pt_resSeg_muCB_1",   ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muSAIO_1 = new TH2F("m_h_pt_resSeg_muSAIO_1", ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muCBIO_1 = new TH2F("m_h_pt_resSeg_muCBIO_1", ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);

  m_h_pt_resSeg_muL1_2   = new TH2F("m_h_pt_resSeg_muL1_2",   ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muSA_2   = new TH2F("m_h_pt_resSeg_muSA_2",   ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muCB_2   = new TH2F("m_h_pt_resSeg_muCB_2",   ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muSAIO_2 = new TH2F("m_h_pt_resSeg_muSAIO_2", ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);
  m_h_pt_resSeg_muCBIO_2 = new TH2F("m_h_pt_resSeg_muCBIO_2", ";[mm];p_{T} [GeV]", 60, -600, 600, 20, 0, 50);

  // number of SPs
  m_h_pt_numberOfSPs_muL1     = new TH2F("m_h_pt_numberOfSPs_muL1",     ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muSA     = new TH2F("m_h_pt_numberOfSPs_muSA",     ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muCB     = new TH2F("m_h_pt_numberOfSPs_muCB",     ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muSAIO   = new TH2F("m_h_pt_numberOfSPs_muSAIO",   ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muCBIO   = new TH2F("m_h_pt_numberOfSPs_muCBIO",   ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);

  m_h_pt_numberOfSPs_muL1_1   = new TH2F("m_h_pt_numberOfSPs_muL1_1",   ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muSA_1   = new TH2F("m_h_pt_numberOfSPs_muSA_1",   ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muCB_1   = new TH2F("m_h_pt_numberOfSPs_muCB_1",   ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muSAIO_1 = new TH2F("m_h_pt_numberOfSPs_muSAIO_1", ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muCBIO_1 = new TH2F("m_h_pt_numberOfSPs_muCBIO_1", ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);

  m_h_pt_numberOfSPs_muL1_2   = new TH2F("m_h_pt_numberOfSPs_muL1_2",   ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muSA_2   = new TH2F("m_h_pt_numberOfSPs_muSA_2",   ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muCB_2   = new TH2F("m_h_pt_numberOfSPs_muCB_2",   ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muSAIO_2 = new TH2F("m_h_pt_numberOfSPs_muSAIO_2", ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);
  m_h_pt_numberOfSPs_muCBIO_2 = new TH2F("m_h_pt_numberOfSPs_muCBIO_2", ";p_{T} [GeV];", 100, 0, 50, 100, -2, 10);

  m_h_pt_numberOfFTFs     = new TH2F("m_h_pt_numberOfFTFs",  ";p_{T} [GeV]", 100, 0, 50, 50, 0, 10);
  m_h_pt_numberOfFTFs1    = new TH2F("m_h_pt_numberOfFTFs1", ";p_{T} [GeV]", 100, 0, 50, 50, 0, 10);
  m_h_pt_numberOfFTFs2    = new TH2F("m_h_pt_numberOfFTFs2", ";p_{T} [GeV]", 100, 0, 50, 50, 0, 10);


  m_h_dR_2muL1       = new TH1F("m_h_dR_2muL1",       "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSA       = new TH1F("m_h_dR_2muSA",       "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCB       = new TH1F("m_h_dR_2muCB",       "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSAIO     = new TH1F("m_h_dR_2muSAIO",     "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCBIO     = new TH1F("m_h_dR_2muCBIO",     "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2mutmp1     = new TH1F("m_h_dR_2mutmp1",     "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2mutmp2     = new TH1F("m_h_dR_2mutmp2",     "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2mutmp3     = new TH1F("m_h_dR_2mutmp3",     "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2mutmp4     = new TH1F("m_h_dR_2mutmp4",     "", nbin_dR, xmin_dR, xmax_dR);

  m_h_dR_2muL1_sep   = new TH1F("m_h_dR_2muL1_sep",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSA_sep   = new TH1F("m_h_dR_2muSA_sep",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCB_sep   = new TH1F("m_h_dR_2muCB_sep",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSAIO_sep = new TH1F("m_h_dR_2muSAIO_sep", "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCBIO_sep = new TH1F("m_h_dR_2muCBIO_sep", "", nbin_dR, xmin_dR, xmax_dR);

  m_h_dR_2muL1_sep1   = new TH1F("m_h_dR_2muL1_sep1",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSA_sep1   = new TH1F("m_h_dR_2muSA_sep1",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCB_sep1   = new TH1F("m_h_dR_2muCB_sep1",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSAIO_sep1 = new TH1F("m_h_dR_2muSAIO_sep1", "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCBIO_sep1 = new TH1F("m_h_dR_2muCBIO_sep1", "", nbin_dR, xmin_dR, xmax_dR);

  m_h_dR_2muL1_sep2   = new TH1F("m_h_dR_2muL1_sep2",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSA_sep2   = new TH1F("m_h_dR_2muSA_sep2",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCB_sep2   = new TH1F("m_h_dR_2muCB_sep2",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSAIO_sep2 = new TH1F("m_h_dR_2muSAIO_sep2", "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCBIO_sep2 = new TH1F("m_h_dR_2muCBIO_sep2", "", nbin_dR, xmin_dR, xmax_dR);

  m_h_dR_2muL1_sep3   = new TH1F("m_h_dR_2muL1_sep3",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSA_sep3   = new TH1F("m_h_dR_2muSA_sep3",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCB_sep3   = new TH1F("m_h_dR_2muCB_sep3",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSAIO_sep3 = new TH1F("m_h_dR_2muSAIO_sep3", "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCBIO_sep3 = new TH1F("m_h_dR_2muCBIO_sep3", "", nbin_dR, xmin_dR, xmax_dR);

  m_h_dR_2muL1_sep4   = new TH1F("m_h_dR_2muL1_sep4",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSA_sep4   = new TH1F("m_h_dR_2muSA_sep4",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCB_sep4   = new TH1F("m_h_dR_2muCB_sep4",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSAIO_sep4 = new TH1F("m_h_dR_2muSAIO_sep4", "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCBIO_sep4 = new TH1F("m_h_dR_2muCBIO_sep4", "", nbin_dR, xmin_dR, xmax_dR);

  m_h_dR_2muL1_sep5   = new TH1F("m_h_dR_2muL1_sep5",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSA_sep5   = new TH1F("m_h_dR_2muSA_sep5",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCB_sep5   = new TH1F("m_h_dR_2muCB_sep5",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSAIO_sep5 = new TH1F("m_h_dR_2muSAIO_sep5", "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCBIO_sep5 = new TH1F("m_h_dR_2muCBIO_sep5", "", nbin_dR, xmin_dR, xmax_dR);

  m_h_dR_2muL1_sep6   = new TH1F("m_h_dR_2muL1_sep6",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSA_sep6   = new TH1F("m_h_dR_2muSA_sep6",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCB_sep6   = new TH1F("m_h_dR_2muCB_sep6",   "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muSAIO_sep6 = new TH1F("m_h_dR_2muSAIO_sep6", "", nbin_dR, xmin_dR, xmax_dR);
  m_h_dR_2muCBIO_sep6 = new TH1F("m_h_dR_2muCBIO_sep6", "", nbin_dR, xmin_dR, xmax_dR);


  for ( int i = 0; i< 8; i++){
    m_h_extFlag.push_back(new TH1F(Form("extFlag_%d", i), "extFlag;Flag;Counts", 100, -20,  20));
    m_h_ptFtk.push_back(new TH1F(Form("ptFtk_%d",     i), "ptFtk;pT;Counts",     100, 0, 10));
    m_h_etaFtk.push_back(new TH1F(Form("etaFtk_%d",   i), "etaFtk;eta;Counts",   100, -2.7, 2.7));
    m_h_phiFtk.push_back(new TH1F(Form("phiFtk_%d",   i), "phiFtk;phi;Counts",   100, -3.2, 3.2));
  }

  m_tree = new TTree("MyTree", "Ntuple");

  m_tree->Branch("nMuons", &m_nMuons, "nMuons/I");
  m_tree->Branch("nFtks", &m_nFtks, "nFtks/I");
  m_tree->Branch("eventNumber", &m_eventNumber, "eventNumber/I");
  m_tree->Branch("runNumber", &m_runNumber, "runNumber/I");
  m_tree->Branch("lumiBlock", &m_lumiBlock, "lumiBlock/I");
  m_tree->Branch("averageInteractionsPerCrossing", &m_averageInteractionsPerCrossing, "averageInteractionsPerCrossing/F");

  // offline
  m_muon_author = new std::vector<int>;
  m_tree->Branch("muon_author", &m_muon_author);

  m_muon_type = new std::vector<int>;
  m_tree->Branch("muon_type", &m_muon_type);

  m_muon_phi = new std::vector<double>;
  m_tree->Branch("muon_phi", &m_muon_phi);

  m_muon_eta = new std::vector<double>;
  m_tree->Branch("muon_eta", &m_muon_eta);

  m_muon_extphi = new std::vector<double>;
  m_tree->Branch("muon_phi", &m_muon_extphi);

  m_muon_exteta = new std::vector<double>;
  m_tree->Branch("muon_eta", &m_muon_exteta);

  m_muon_charge = new std::vector<double>;
  m_tree->Branch("muon_charge", &m_muon_charge);

  m_muon_pt = new std::vector<double>;
  m_tree->Branch("muon_pt", &m_muon_pt);

  m_ftf_size = new std::vector<int>;
  m_tree->Branch("ftf_size", &m_ftf_size);

  m_ftf_size1 = new std::vector<int>;
  m_tree->Branch("ftf_size1", &m_ftf_size1);

  m_ftf_size2 = new std::vector<int>;
  m_tree->Branch("ftf_size2", &m_ftf_size2);

  // l1
  m_l1_pt = new std::vector<double>;
  m_tree->Branch("l1_pt", &m_l1_pt);

  m_l1_eta = new std::vector<double>;
  m_tree->Branch("l1_eta", &m_l1_eta);

  m_l1_phi = new std::vector<double>;
  m_tree->Branch("l1_phi", &m_l1_phi);


  // l2sa
  m_l2sa_pt = new std::vector<double>;
  m_tree->Branch("l2sa_pt", &m_l2sa_pt);

  m_l2sa_eta = new std::vector<double>;
  m_tree->Branch("l2sa_eta", &m_l2sa_eta);

  m_l2sa_phi = new std::vector<double>;
  m_tree->Branch("l2sa_phi", &m_l2sa_phi);

  // l2cb
  m_l2cb_pt = new std::vector<double>;
  m_tree->Branch("l2cb_pt", &m_l2cb_pt);

  m_l2cb_eta = new std::vector<double>;
  m_tree->Branch("l2cb_eta", &m_l2cb_eta);

  m_l2cb_phi = new std::vector<double>;
  m_tree->Branch("l2cb_phi", &m_l2cb_phi);

  // l2saio
  m_l2saio_pt = new std::vector<double>;
  m_tree->Branch("l2saio_pt", &m_l2saio_pt);

  m_l2saio_eta = new std::vector<double>;
  m_tree->Branch("l2saio_eta", &m_l2saio_eta);

  m_l2saio_phi = new std::vector<double>;
  m_tree->Branch("l2saio_phi", &m_l2saio_phi);

  // l2cbio
  m_l2cbio_pt = new std::vector<double>;
  m_tree->Branch("l2cbio_pt", &m_l2cbio_pt);

  m_l2cbio_eta = new std::vector<double>;
  m_tree->Branch("l2cbio_eta", &m_l2cbio_eta);

  m_l2cbio_phi = new std::vector<double>;
  m_tree->Branch("l2cbio_phi", &m_l2cbio_phi);

  // pass
  m_l1_pass = new std::vector<int>;
  m_tree->Branch("l1_pass", &m_l1_pass);

  m_l2sa_pass = new std::vector<int>;
  m_tree->Branch("l2sa_pass", &m_l2sa_pass);

  m_l2cb_pass = new std::vector<int>;
  m_tree->Branch("l2cb_pass", &m_l2cb_pass);

  m_l2saio_pass = new std::vector<int>;
  m_tree->Branch("l2saio_pass", &m_l2saio_pass);

  m_l2cbio_pass = new std::vector<int>;
  m_tree->Branch("l2cbio_pass", &m_l2cbio_pass);


  m_l1_numberOfSPs0     = new std::vector<int>;
  m_l2sa_numberOfSPs0   = new std::vector<int>;
  m_l2cb_numberOfSPs0   = new std::vector<int>;
  m_l2saio_numberOfSPs0 = new std::vector<int>;
  m_l2cbio_numberOfSPs0 = new std::vector<int>;

  m_l1_numberOfSPs1     = new std::vector<int>;
  m_l2sa_numberOfSPs1   = new std::vector<int>;
  m_l2cb_numberOfSPs1   = new std::vector<int>;
  m_l2saio_numberOfSPs1 = new std::vector<int>;
  m_l2cbio_numberOfSPs1 = new std::vector<int>;

  m_l1_resSeg_0     = new std::vector<double>;
  m_l2sa_resSeg_0   = new std::vector<double>;
  m_l2cb_resSeg_0   = new std::vector<double>;
  m_l2saio_resSeg_0 = new std::vector<double>;
  m_l2cbio_resSeg_0 = new std::vector<double>;

  m_l1_resSeg_1     = new std::vector<double>;
  m_l2sa_resSeg_1   = new std::vector<double>;
  m_l2cb_resSeg_1   = new std::vector<double>;
  m_l2saio_resSeg_1 = new std::vector<double>;
  m_l2cbio_resSeg_1 = new std::vector<double>;

  m_l1_resSeg_2     = new std::vector<double>;
  m_l2sa_resSeg_2   = new std::vector<double>;
  m_l2cb_resSeg_2   = new std::vector<double>;
  m_l2saio_resSeg_2 = new std::vector<double>;
  m_l2cbio_resSeg_2 = new std::vector<double>;

  m_ftk_phi = new std::vector<double>;
  m_tree->Branch("ftk_phi", &m_ftk_phi);

  m_ftk_eta = new std::vector<double>;
  m_tree->Branch("ftk_eta", &m_ftk_eta);

  m_ftk_pt = new std::vector<double>;
  m_tree->Branch("ftk_pt", &m_ftk_pt);

  m_ftk_charge = new std::vector<double>;
  m_tree->Branch("ftk_charge", &m_ftk_charge);

  m_ftk_d0 = new std::vector<double>;
  m_tree->Branch("ftk_d0", &m_ftk_d0);

  m_ftk_z0 = new std::vector<double>;
  m_tree->Branch("ftk_z0", &m_ftk_z0);

  m_tree->Print();

  ATH_MSG_INFO("CalcEffAlg::initialize() end");

  return StatusCode::SUCCESS;
}

StatusCode CalcEffAlg::finalize() {
  ATH_MSG_INFO("finalize()");
  m_file->Write();

  delete m_muon_author; m_muon_author = 0;
  delete m_muon_type; m_muon_type = 0;
  delete m_muon_phi; m_muon_phi = 0;
  delete m_muon_eta; m_muon_eta = 0;
  delete m_muon_extphi; m_muon_extphi = 0;
  delete m_muon_exteta; m_muon_exteta = 0;
  delete m_muon_charge; m_muon_charge = 0;
  delete m_muon_pt; m_muon_pt = 0;

  delete m_ftf_size; m_ftf_size = 0;
  delete m_ftf_size1; m_ftf_size1 = 0;
  delete m_ftf_size2; m_ftf_size2 = 0;

  delete m_l1_pt; m_l1_pt = 0;
  delete m_l1_eta; m_l1_eta = 0;
  delete m_l1_phi; m_l1_phi = 0;

  delete m_l2sa_pt; m_l2sa_pt = 0;
  delete m_l2sa_eta; m_l2sa_eta = 0;
  delete m_l2sa_phi; m_l2sa_phi = 0;

  delete m_l2cb_pt; m_l2cb_pt = 0;
  delete m_l2cb_eta; m_l2cb_eta = 0;
  delete m_l2cb_phi; m_l2cb_phi = 0;

  delete m_l2saio_pt; m_l2saio_pt = 0;
  delete m_l2saio_eta; m_l2saio_eta = 0;
  delete m_l2saio_phi; m_l2saio_phi = 0;

  delete m_l2cbio_pt; m_l2cbio_pt = 0;
  delete m_l2cbio_eta; m_l2cbio_eta = 0;
  delete m_l2cbio_phi; m_l2cbio_phi = 0;

  delete m_l1_pass; m_l1_pass = 0;
  delete m_l2sa_pass; m_l2sa_pass = 0;
  delete m_l2cb_pass; m_l2cb_pass = 0;
  delete m_l2saio_pass; m_l2saio_pass = 0;
  delete m_l2cbio_pass; m_l2cbio_pass = 0;


  delete m_l1_numberOfSPs0; m_l1_numberOfSPs0 = 0;
  delete m_l2sa_numberOfSPs0; m_l2sa_numberOfSPs0 = 0;
  delete m_l2cb_numberOfSPs0; m_l2cb_numberOfSPs0 = 0;
  delete m_l2saio_numberOfSPs0; m_l2saio_numberOfSPs0 = 0;
  delete m_l2cbio_numberOfSPs0; m_l2cbio_numberOfSPs0 = 0;

  delete m_l1_numberOfSPs1; m_l1_numberOfSPs1 = 0;
  delete m_l2sa_numberOfSPs1; m_l2sa_numberOfSPs1 = 0;
  delete m_l2cb_numberOfSPs1; m_l2cb_numberOfSPs1 = 0;
  delete m_l2saio_numberOfSPs1; m_l2saio_numberOfSPs1 = 0;
  delete m_l2cbio_numberOfSPs1; m_l2cbio_numberOfSPs1 = 0;

  delete m_l1_resSeg_1; m_l1_resSeg_1 = 0;
  delete m_l2sa_resSeg_1; m_l2sa_resSeg_1 = 0;
  delete m_l2cb_resSeg_1; m_l2cb_resSeg_1 = 0;
  delete m_l2saio_resSeg_1; m_l2saio_resSeg_1 = 0;
  delete m_l2cbio_resSeg_1; m_l2cbio_resSeg_1 = 0;

  delete m_l1_resSeg_2; m_l1_resSeg_2 = 0;
  delete m_l2sa_resSeg_2; m_l2sa_resSeg_2 = 0;
  delete m_l2cb_resSeg_2; m_l2cb_resSeg_2 = 0;
  delete m_l2saio_resSeg_2; m_l2saio_resSeg_2 = 0;
  delete m_l2cbio_resSeg_2; m_l2cbio_resSeg_2 = 0;

  delete m_l1_resSeg_0; m_l1_resSeg_0 = 0;
  delete m_l2sa_resSeg_0; m_l2sa_resSeg_0 = 0;
  delete m_l2cb_resSeg_0; m_l2cb_resSeg_0 = 0;
  delete m_l2saio_resSeg_0; m_l2saio_resSeg_0 = 0;
  delete m_l2cbio_resSeg_0; m_l2cbio_resSeg_0 = 0;

  delete m_ftk_phi; m_ftk_phi = 0;
  delete m_ftk_eta; m_ftk_eta = 0;
  delete m_ftk_pt; m_ftk_pt = 0;
  delete m_ftk_charge; m_ftk_charge = 0;
  delete m_ftk_d0; m_ftk_d0 = 0;
  delete m_ftk_z0; m_ftk_z0 = 0;

  return StatusCode::SUCCESS;
}

StatusCode CalcEffAlg::execute() {
  const float ZERO_LIMIT = 1.e-10;
  ATH_MSG_INFO("execute()");

  auto ctx = getContext();
  ATH_MSG_INFO("Get event context << " << ctx );

  //SG::ReadHandle<xAOD::L2StandAloneMuonContainer> muonSAs(m_L2MuonSAContainerKey, ctx);
  //if (! muonSAs.isValid() ) {
  //  ATH_MSG_ERROR("evtStore() does not contain xAOD::L2StandAloneMuon collection with name "<< m_L2MuonSAContainerKey);
  //  return StatusCode::FAILURE;
  //}

  ///main function to do "Tag and Probe" .

  //==============================================================
  //=  Event information
  //==============================================================
  const xAOD::EventInfo* eventInfo = 0;
  StatusCode sc = evtStore()->retrieve(eventInfo);
  if(StatusCode::SUCCESS!=sc || !eventInfo) {
    ATH_MSG_WARNING("Could not retrieve EventInfo");
    return StatusCode::SUCCESS;
  }

  uint32_t runNumber = eventInfo->runNumber();
  unsigned long long eventNumber = eventInfo->eventNumber();
  //int lumiBlock = eventInfo-> lumiBlock();
  double averageInteractionsPerCrossing =  eventInfo -> averageInteractionsPerCrossing();
  ATH_MSG_INFO("===================================================");
  ATH_MSG_INFO("===================================================");
  ATH_MSG_INFO("eventNumber==========#" << eventNumber << "========" );
  ATH_MSG_INFO("===================================================");
  ATH_MSG_INFO("===================================================");
  ATH_MSG_INFO("Run = " << runNumber << " : Event = " << eventNumber << " : mu = " << averageInteractionsPerCrossing );
  bool isMC = true;
  if(!eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION ) ){
    isMC = false;
  }

  //checkFtk().setChecked();


  // GRL
  if( !isMC && m_useGRL ){
    ATH_MSG_INFO("Skip this event via GRL");
    if(!m_grlTool->passRunLB(*eventInfo)) return StatusCode::SUCCESS; //checks the GRL and skips to next event if not passing
  } // end if not MC

  m_muon_author->clear();
  m_muon_type->clear();
  m_muon_phi->clear();
  m_muon_charge->clear();
  m_muon_eta->clear();
  m_muon_pt->clear();
  m_muon_exteta->clear();
  m_muon_extphi->clear();

  m_ftf_size->clear();
  m_ftf_size1->clear();
  m_ftf_size2->clear();

  m_l1_pt->clear();
  m_l1_eta->clear();
  m_l1_phi->clear();

  m_l2sa_pt->clear();
  m_l2sa_eta->clear();
  m_l2sa_phi->clear();

  m_l2cb_pt->clear();
  m_l2cb_eta->clear();
  m_l2cb_phi->clear();

  m_l2saio_pt->clear();
  m_l2saio_eta->clear();
  m_l2saio_phi->clear();

  m_l2cbio_pt->clear();
  m_l2cbio_eta->clear();
  m_l2cbio_phi->clear();

  m_l1_pass->clear();
  m_l2sa_pass->clear();
  m_l2cb_pass->clear();
  m_l2saio_pass->clear();
  m_l2cbio_pass->clear();

  m_l1_numberOfSPs0->clear();
  m_l2sa_numberOfSPs0->clear();
  m_l2cb_numberOfSPs0->clear();
  m_l2saio_numberOfSPs0->clear();
  m_l2cbio_numberOfSPs0->clear();

  m_l1_numberOfSPs1->clear();
  m_l2sa_numberOfSPs1->clear();
  m_l2cb_numberOfSPs1->clear();
  m_l2saio_numberOfSPs1->clear();
  m_l2cbio_numberOfSPs1->clear();

  m_l1_resSeg_0->clear();
  m_l2sa_resSeg_0->clear();
  m_l2cb_resSeg_0->clear();
  m_l2saio_resSeg_0->clear();
  m_l2cbio_resSeg_0->clear();

  m_l1_resSeg_1->clear();
  m_l2sa_resSeg_1->clear();
  m_l2cb_resSeg_1->clear();
  m_l2saio_resSeg_1->clear();
  m_l2cbio_resSeg_1->clear();

  m_l1_resSeg_2->clear();
  m_l2sa_resSeg_2->clear();
  m_l2cb_resSeg_2->clear();
  m_l2saio_resSeg_2->clear();
  m_l2cbio_resSeg_2->clear();

  m_ftk_phi->clear();
  m_ftk_eta->clear();
  m_ftk_pt->clear();
  m_ftk_charge->clear();
  m_ftk_d0->clear();
  m_ftk_z0->clear();

  // ===== retrieve RoI muons
  const xAOD::MuonRoIContainer* rois = 0;
  std::string L1Key = "LVL1MuonRoIs";
  ATH_CHECK( evtStore()->retrieve( rois, L1Key ) );
  ATH_MSG_INFO(L1Key << " size = " << rois->size());

  // ===== retrieve l2samuons
  const xAOD::L2StandAloneMuonContainer* l2samuons = 0;
  //ATH_CHECK( evtStore()->retrieve( l2samuons, "HLT_xAOD__L2StandAloneMuonContainer_MuonL2SAInfo" ) );
  std::string saKey = "HLT_MuonL2SAInfo";
  ATH_CHECK( evtStore()->retrieve( l2samuons, saKey ) );

  // retrieve l2samuons for IOmode
  const xAOD::L2StandAloneMuonContainer* l2samuonIOs = 0;
  //ATH_CHECK( evtStore()->retrieve( l2samuons, "HLT_xAOD__L2StandAloneMuonContainer_MuonL2SAInfo" ) );
  std::string saIOKey = "HLT_MuonL2SAInfoIOmode";
  ATH_CHECK( evtStore()->retrieve( l2samuonIOs, saIOKey ) );

  // retrieve l2cbmuons
  const xAOD::L2CombinedMuonContainer* l2cbmuons = 0;
  std::string cbKey = "HLT_MuonL2CBInfo";
  ATH_CHECK( evtStore()->retrieve( l2cbmuons, cbKey ) );

  // retrieve l2cbmuons for IOmode
  const xAOD::L2CombinedMuonContainer* l2cbmuonIOs = 0;
  //ATH_CHECK( evtStore()->retrieve( l2cbmuons, "HLT_xAOD__L2StandAloneMuonContainer_MuonL2CBInfo" ) );
  std::string cbIOKey = "HLT_MuonL2CBInfoIOmode";
  ATH_CHECK( evtStore()->retrieve( l2cbmuonIOs, cbIOKey ) );

  // ===== retrieve offline muons
  const xAOD::MuonContainer* muons = 0;
  ATH_CHECK( evtStore()->retrieve( muons, "Muons" ) );


  m_nMuons = muons->size();
  ATH_MSG_INFO("Muon size=" << muons->size());
  ATH_MSG_INFO(saKey << " size = " << l2samuons->size());
  ATH_MSG_INFO(cbKey << " size = " << l2cbmuons->size());
  ATH_MSG_INFO(saIOKey << " size = " << l2samuonIOs->size());
  ATH_MSG_INFO(cbIOKey << " size = " << l2cbmuonIOs->size());


  // Jpsi selection
  TLorentzVector lvmu1, lvmu2, lvdimu;
  double dimuMass = -999999;

  //const xAOD::L2StandAloneMuonContainer* m_l2sas = 0;

  //double muon1pt  = 0.;
  //double muon2pt  = 0.;
  //double muon1eta = 0.;
  //double muon2eta = 0.;
  //double muon1phi = 0.;
  //double muon2phi = 0.;

  double pt_nL2SAIOs = 3.;
  double pt_nL2SAIOs_highPt = 10.0;

  int imuon = -1;
  int jmuon = -1;
  bool matchJpsi = false;
  for ( unsigned int i = 0; i < muons->size(); i++ ){
    const xAOD::Muon* muon1 = muons->at(i);
    for ( unsigned int j = 0; j < muons->size(); j++ ){
      const xAOD::Muon* muon2 = muons->at(j);
      lvmu1.SetPtEtaPhiM( muon1->pt(), muon1->eta(), muon1->phi(), 105.6 );
      lvmu2.SetPtEtaPhiM( muon2->pt(), muon2->eta(), muon2->phi(), 105.6 );
      lvdimu = lvmu1 + lvmu2;
      double tmp_dimuMass = lvdimu.M();
      //const double dimuDphi = lvmu1.DeltaPhi( lvmu2 );
      ATH_MSG_INFO("i/j Muon1 : " << i << "/" << j << "/ " << (muon1->pt())/1000. << ", Muon2 : " << (muon2->pt())/1000.);
      ATH_MSG_INFO("Mass:  " << tmp_dimuMass);
      if ( (3099.-300.) < tmp_dimuMass && tmp_dimuMass < (3099.+300.) && abs(muon1->pt()-muon2->pt()) > ZERO_LIMIT){
        if (muon1->charge() != muon2->charge()) {
          if ( (muon1->author() == xAOD::Muon::MuidCo || muon1->author() == xAOD::Muon::STACO) && (muon2->author() == xAOD::Muon::MuidCo || muon2->author() == xAOD::Muon::STACO)) {
            if ( (muon1->muonType()==xAOD::Muon::Combined || muon1->muonType()==xAOD::Muon::SegmentTagged) && (muon2->muonType()==xAOD::Muon::Combined || muon2->muonType()==xAOD::Muon::SegmentTagged) ) {
              if ( (muon1->quality()==xAOD::Muon::Medium || muon1->quality()==xAOD::Muon::Tight) && (muon2->quality()==xAOD::Muon::Medium || muon2->quality()==xAOD::Muon::Tight) ){
                ATH_MSG_INFO("Jpsi");
                //muon1pt = muon1->pt();
                //muon2pt = muon1->pt();
                //muon1eta = muon1->eta();
                //muon2eta = muon1->eta();
                //muon1phi = muon1->phi();
                //muon2phi = muon1->phi();
                ATH_MSG_INFO("muon1 pt/eta/phi: " << (muon1->pt())/1000. << "/" << muon1->eta() << "/" << muon1->phi());
                ATH_MSG_INFO("muon2 pt/eta/phi: " << (muon2->pt())/1000. << "/" << muon2->eta() << "/" << muon2->phi());
                matchJpsi = true;
                imuon = i;
                jmuon = j;
                dimuMass = tmp_dimuMass;
              }
            }
          }
        }
      }
      if (matchJpsi) break;
      jmuon++;
    }
    if (matchJpsi) break;
    imuon++;
  }

  if (matchJpsi){
    ATH_MSG_INFO("match imuon/jmuon : " << imuon << "/" << jmuon << ": " << dimuMass);
    //m_h_dimumass->Fill(dimuMass/1000.);
  } else {
    ATH_MSG_INFO("not match, no Jpsi muons" << imuon << "/" << jmuon << ": " << dimuMass);
  }

  for ( int k = 0; (unsigned int)k < muons->size(); k++ ) {
    if ( k != imuon && k != jmuon ) continue;
    if (!matchJpsi) continue;
    ATH_MSG_INFO("k = " << k);
    const xAOD::Muon* muon = muons->at(k);
    int matchedRoINumber = -1;
    double matchedRoIEta = -9999.;
    int isPassedL1 = -1;
    int isPassedSA = -1;
    int isPassedCB = -1;
    int isPassedSAIO = -1;
    int isPassedCBIO = -1;

    const xAOD::MuonRoI* matchedRoI                 = NULL;
    const xAOD::L2StandAloneMuon* matchedL2SAMuon   = NULL;
    const xAOD::L2CombinedMuon* matchedL2CBMuon     = NULL;
    const xAOD::L2StandAloneMuon* matchedL2SAMuonIO = NULL;
    const xAOD::L2CombinedMuon* matchedL2CBMuonIO   = NULL;


    // extrapolate
    const xAOD::TrackParticle* mutrk = muon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
    pair< double, double > extEtaAndPhi;
    if (mutrk) extEtaAndPhi = m_ext.extTrack( mutrk );
    const double muExtEta = ( mutrk )? extEtaAndPhi.first:muon->eta();
    const double muExtPhi = ( mutrk )? extEtaAndPhi.second:muon->phi();
    ATH_MSG_INFO("offline muon loop");
    ATH_MSG_INFO("Offline pt/eta/phi=" << (muon->pt())/1000. << "/" << muon->eta() << "/" << muon->phi());
    ATH_MSG_INFO("mutrk (extrapolated muon) eta/phi = " << muExtEta << ", " << muExtPhi);

    const double muPt   = muon->pt();
    const double muEta  = muon->eta();
    const double muPhi  = muon->phi();

    // L1 dR match
    CalcEffAlg::L1Item trigL1 = L1_MU4;
    double reqdR = ( muPt < 1000.) ? -0.00001*muPt + 0.18 : 0.08;
    for( const auto& roi : *rois){
      int roiThr       = roi->getThrNumber();
      double roiEta    = roi->eta();
      double roiPhi    = roi->phi();
      const int roiNum = roi->getRoI();
      const int roiPt  = roi->thrValue();
      ATH_MSG_INFO(" L1: roiNum = " << roiNum << ", roiThr = " << roiThr << ", roiPt = " << roiPt << ", roiEta = " << roiEta << ", roiPhi = " << roiPhi);
      // Check dR
      const double roidRex  = deltaR( muExtEta, muExtPhi, roiEta, roiPhi);
      bool isdR = roidRex < reqdR;
      ATH_MSG_INFO(" isdR = " << isdR << ", reqdR = " << reqdR << ", roidRex = " << roidRex);
      // Check L1 pass
      if( (roiThr >= trigL1) && isdR ){
        matchedRoINumber = roiNum;
        reqdR = roidRex;
        matchedRoIEta = roiEta;
        isPassedL1 = 1;
        ATH_MSG_INFO(" Matched in L1");
        matchedRoI = roi;
      } else {
        ATH_MSG_INFO(" Not Matched in L1");
      }
    } // roi loop
    ATH_MSG_INFO("isPassedL1:" << isPassedL1 << ", L1 roiNum: " << matchedRoINumber << ", L1 roiEta: " << matchedRoIEta);


    // L2SA RoI number match
    for( const auto& l2samuon : *l2samuons ) {
      int l2saRoINum  = l2samuon->roiNumber();
      double l2saRoIEta  = l2samuon->roiEta();
      if ( abs(l2saRoIEta - matchedRoIEta) < ZERO_LIMIT){
        if (abs(l2samuon->pt()) > 3.38){
          ATH_MSG_INFO(" matched&pT L2SA roiNumb/pt/eta/phi/roiEta/roiPhi = " << l2samuon->roiNumber() << "/" << l2samuon->pt() << "/" << l2samuon->eta() << "/" << l2samuon->phi() << "/" << l2samuon->roiEta() << "/" << l2samuon->roiPhi());
          isPassedSA = 1;
          matchedL2SAMuon = l2samuon;
        } else {
          ATH_MSG_INFO(" matched L2SA roiNumb/pt/eta/phi/roiEta/roiPhi = " << l2samuon->roiNumber() << "/" << l2samuon->pt() << "/" << l2samuon->eta() << "/" << l2samuon->phi() << "/" << l2samuon->roiEta() << "/" << l2samuon->roiPhi());
          matchedL2SAMuon = l2samuon;
        }
      } else {
        ATH_MSG_INFO(" non-matched L2SA roiNumb/pt/eta/phi/roiEta/roiPhi = " << l2samuon->roiNumber() << "/" << l2samuon->pt() << "/" << l2samuon->eta() << "/" << l2samuon->phi() << "/" << l2samuon->roiEta() << "/" << l2samuon->roiPhi());
      }
    }
    ATH_MSG_INFO("isPassedSA:" << isPassedSA << ", SA roiNum: " << matchedRoINumber << ", SA roiEta: " << matchedRoIEta);

    // L2CB RoI number match
    for( const auto& l2cbmuon : *l2cbmuons ) {
      const xAOD::L2StandAloneMuon* L2SAMuon = NULL;
      ATH_MSG_INFO(" L2CB pt/eta/phi=" << (l2cbmuon->pt())/1000. << "/" << l2cbmuon->eta() << "/" << l2cbmuon->phi());
      const ElementLink< xAOD::L2StandAloneMuonContainer >& Link  = l2cbmuon->muSATrackLink();
      int l2cbRoINum  = -99999;
      double l2cbRoIEta  = -99999;
      if (Link.isValid()) L2SAMuon = *Link;
      if (L2SAMuon == NULL) {
        ATH_MSG_INFO(" No muSATrackLink" );   
      } else {
        ATH_MSG_INFO(" muSATrackLink = " << L2SAMuon->roiNumber() );
        l2cbRoINum = L2SAMuon->roiNumber();
        l2cbRoIEta = L2SAMuon->roiEta();
      }
      if ( abs(l2cbRoIEta - matchedRoIEta) < ZERO_LIMIT ){
        if (abs(l2cbmuon->pt())/1000. > 3.86){
          ATH_MSG_INFO(" matched&pT L2CB pt/eta/phi = " << (l2cbmuon->pt())/1000. << "/" << l2cbmuon->eta() << "/" << l2cbmuon->phi());
          isPassedCB = 1;
          matchedL2CBMuon = l2cbmuon;
        } else {
          ATH_MSG_INFO(" matched&pT L2CB pt/eta/phi = " << (l2cbmuon->pt())/1000. << "/" << l2cbmuon->eta() << "/" << l2cbmuon->phi());
          matchedL2CBMuon = l2cbmuon;
        }
      } else {
        ATH_MSG_INFO(" non-matched L2CB pt/eta/phi = " << (l2cbmuon->pt())/1000. << "/" << l2cbmuon->eta() << "/" << l2cbmuon->phi());
      }
    }

    ATH_MSG_INFO("isPassedCB:" << isPassedCB << ", CB roiNum: " << matchedRoINumber << ", CB roiEta: " << matchedRoIEta);

    // L2SAIO roiEta match
    ATH_MSG_INFO(saIOKey << "size = " << l2samuonIOs->size());
    double mindR = 999999;
    double lowdr = 0.1;
    int min_counter = -1;
    int tmp_counter = 0;
    int nL2SAIOs = 0;
    int nL2SAIOs_highpt = 0;
    int nL2SAIOs_lowdr = 0;
    for ( const auto& l2samuonIO : *l2samuonIOs ) {
      double dR_off_l2samuonIO = deltaR(muExtEta,muExtPhi,l2samuonIO->etaMS(),l2samuonIO->phiMS());
      double dR_off_l2cbmuonIO = deltaR(muEta,muPhi,l2cbmuonIOs->at(tmp_counter)->eta(),l2cbmuonIOs->at(tmp_counter)->phi());
      ATH_MSG_INFO(" L2SAIO pt/eta/phi/roiEta/roiPhi=" << l2samuonIO->pt() << "/" << l2samuonIO->eta() << "/" << l2samuonIO->phi() << "/" << l2samuonIO->roiEta() << "/" << l2samuonIO->roiPhi());
      ATH_MSG_INFO(" l2saIO NumberOfSP: " << NumberOfSP(l2samuonIO));
      ATH_MSG_INFO(" L2SAIO dR/ L2CBIO dR: " << dR_off_l2samuonIO << "/" << dR_off_l2cbmuonIO);
      double matchedL2SAMuon_roiEta = (matchedL2SAMuon)? (matchedL2SAMuon->roiEta()):-99999;
      if ( abs( l2samuonIO->roiEta() - matchedL2SAMuon_roiEta ) < ZERO_LIMIT){
        //if ( dR_off_l2samuonIO < mindR ){
        //  mindR = dR_off_l2samuonIO;
        //  min_counter = tmp_counter;
        //}

        if ( abs(l2cbmuonIOs->at(tmp_counter)->pt()) > 3. && abs(muon->eta()) < 1.05 ) m_h_pt_numberOfSPs_muSAIO_2->Fill(l2cbmuonIOs->at(tmp_counter)->pt(),NumberOfSP(l2samuonIO));
        if ( dR_off_l2cbmuonIO < mindR && abs(l2cbmuonIOs->at(tmp_counter)->pt()) > 3.  ){
          mindR = dR_off_l2cbmuonIO;
          min_counter = tmp_counter;
        }
      }

      // count number FTF tracks in RoI which is matched with offline and L2SA
      ATH_MSG_INFO("abs( l2samuonIO->roiEta() - matchedL2SAMuon_roiEta ) = " << abs( l2samuonIO->roiEta() - matchedL2SAMuon_roiEta ) << "/" << l2samuonIO->roiEta() << "/" << matchedL2SAMuon_roiEta);
      if (abs( l2samuonIO->roiEta() - matchedL2SAMuon_roiEta ) < ZERO_LIMIT){
        //nL2SAIOs++;
        ATH_MSG_INFO("  -> L2CBIOs pT: " << l2cbmuonIOs->at(tmp_counter)->pt() << "/" << l2cbmuonIOs->at(tmp_counter)->eta() << "/" << l2cbmuonIOs->at(tmp_counter)->phi());
        if (l2cbmuonIOs->at(tmp_counter)->pt() > pt_nL2SAIOs) nL2SAIOs++;
        if (l2cbmuonIOs->at(tmp_counter)->pt() > pt_nL2SAIOs_highPt) nL2SAIOs_highpt++;
        if (l2cbmuonIOs->at(tmp_counter)->pt() > pt_nL2SAIOs && dR_off_l2cbmuonIO < lowdr) nL2SAIOs_lowdr++;
      }

      tmp_counter++;
    }
    ATH_MSG_INFO("mindR/min_counter: " << mindR << "/" << min_counter);
    if (min_counter > -1) matchedL2SAMuonIO = l2samuonIOs->at(min_counter);
    if (min_counter > -1 && (l2cbmuonIOs->at(min_counter))) matchedL2CBMuonIO = l2cbmuonIOs->at(min_counter);

    if (min_counter > -1 && (abs(l2samuonIOs->at(min_counter)->pt()) > 3.86)) isPassedSAIO = 1;

    // L2CBIO Elementlink match
    if (min_counter > -1) {
      const xAOD::L2StandAloneMuon* L2SAMuon = NULL;
      const ElementLink< xAOD::L2StandAloneMuonContainer >& Link  = l2cbmuonIOs->at(min_counter)->muSATrackLink();
      if (Link.isValid()) {
        L2SAMuon = *Link;
        ATH_MSG_INFO(matchedL2SAMuonIO->pt() << "/" << matchedL2SAMuonIO->eta() << "/" << L2SAMuon->phi());
        if ( (L2SAMuon->pt() - matchedL2SAMuonIO->pt()) < ZERO_LIMIT && (L2SAMuon->eta() - matchedL2SAMuonIO->eta()) < ZERO_LIMIT && (L2SAMuon->phi() - matchedL2SAMuonIO->phi()) < ZERO_LIMIT ){
          matchedL2CBMuonIO = l2cbmuonIOs->at(min_counter);
        ATH_MSG_INFO("matchedL2SAMuonIO is linked from matchedL2CBMuonIO");
        }
      } else {
        ATH_MSG_INFO("matchedL2SAMuonIO is not linked from matchedL2CBMuonIO");
      }
    }
    if (min_counter > -1 && abs(matchedL2CBMuonIO->pt()) > 3.86) isPassedCBIO = 1;



    // matching result
    ATH_MSG_INFO("matching result");
    ATH_MSG_INFO("isPassedL1/isPassedSA/isPassedCB/isPassedSAIO/isPassedCBIO: " << isPassedL1 << "/" << isPassedSA << "/" << isPassedCB << "/" << isPassedSAIO << "/" << isPassedCBIO);
    ATH_MSG_INFO("offline muon pt/eta/phi: " << (muon->pt())/1000. << "/" << muon->eta() << "/" << muon->phi());
    ATH_MSG_INFO("offline extrapolated muon eta/phi: " <<  muExtEta << "/" << muExtPhi);

    m_muon_eta->push_back(muon->eta());
    m_muon_phi->push_back(muon->phi());
    m_muon_exteta->push_back(muExtEta);
    m_muon_extphi->push_back(muExtPhi);
    m_muon_pt->push_back(muon->pt()/1000.);
    m_muon_charge->push_back(muon->charge());
    m_muon_author->push_back(muon->author());
    m_muon_type->push_back(muon->muonType());

    m_l1_pass->push_back(isPassedL1);
    if ( matchedRoI ) {
      ATH_MSG_INFO("matchedRoI roiThr/roiEta/roiPhi: " << matchedRoI->getThrNumber() << "/" << matchedRoI->eta() << "/" << matchedRoI->phi());
      m_l1_pt->push_back(matchedRoI->getRoI());
      m_l1_eta->push_back(matchedRoI->eta());
      m_l1_phi->push_back(matchedRoI->phi());
      m_h_pt_dR_L1_0->Fill(muon->pt()/1000., deltaR(muon->eta(), muon->phi(), matchedRoI->eta(), matchedRoI->phi()));
    } else {
      m_l1_pt->push_back(0.);
      m_l1_eta->push_back(0.);
      m_l1_phi->push_back(0.);
    }

    m_l2sa_pass->push_back(isPassedSA);
    if ( matchedL2SAMuon ){
      ATH_MSG_INFO("matchedL2SA pt/eta/phi: " << matchedL2SAMuon->pt() << "/" << matchedL2SAMuon->eta() << "/" << matchedL2SAMuon->phi());
      m_l2sa_pt->push_back(matchedL2SAMuon->pt());
      m_l2sa_eta->push_back(matchedL2SAMuon->eta());
      m_l2sa_phi->push_back(matchedL2SAMuon->phi());
      m_l2sa_numberOfSPs0->push_back(NumberOfSP(matchedL2SAMuon));
      m_l2sa_numberOfSPs1->push_back(NumberOfSP(matchedL2SAMuon));

      m_h_pt_alpha->Fill(1/abs(matchedL2SAMuon->pt()), matchedL2SAMuon->endcapAlpha());
      m_h_pt_beta->Fill(1/abs(matchedL2SAMuon->pt()), matchedL2SAMuon->endcapBeta());
      m_h_pt_alpha1->Fill(1/abs(matchedL2SAMuon->ptEndcapAlpha()), matchedL2SAMuon->endcapAlpha());
      m_h_pt_beta1->Fill(1/abs(matchedL2SAMuon->ptEndcapBeta()), matchedL2SAMuon->endcapBeta());
      //m_l2sas->push_back(matchedL2SAMuon);
    } else {
      m_l2sa_pt->push_back(0.);
      m_l2sa_eta->push_back(0.);
      m_l2sa_phi->push_back(0.);
      m_l2sa_numberOfSPs0->push_back(-10);
      m_l2sa_numberOfSPs1->push_back(-10);
    }

    m_l2cb_pass->push_back(isPassedCB);
    if ( matchedL2CBMuon ){
      ATH_MSG_INFO("matchedL2CB pt/eta/phi: " << matchedL2CBMuon->pt() << "/" << matchedL2CBMuon->eta() << "/" << matchedL2CBMuon->phi());
      m_l2cb_pt->push_back((matchedL2CBMuon->pt())/1000.);
      m_l2cb_eta->push_back(matchedL2CBMuon->eta());
      m_l2cb_phi->push_back(matchedL2CBMuon->phi());
    } else {
      m_l2cb_pt->push_back(0.);
      m_l2cb_eta->push_back(0.);
      m_l2cb_phi->push_back(0.);
    }

    m_l2saio_pass->push_back(isPassedSAIO);
    if ( matchedL2SAMuonIO ){
      ATH_MSG_INFO("matchedL2SAMuonIO pt/eta/phi: " << matchedL2SAMuonIO->pt() << "/" << matchedL2SAMuonIO->eta() << "/" << matchedL2SAMuonIO->phi());
      m_l2saio_pt->push_back(matchedL2SAMuonIO->pt());
      m_l2saio_eta->push_back(matchedL2SAMuonIO->eta());
      m_l2saio_phi->push_back(matchedL2SAMuonIO->phi());
      m_l2saio_numberOfSPs0->push_back(NumberOfSP(matchedL2SAMuonIO));
      m_l2saio_numberOfSPs1->push_back(NumberOfSP(matchedL2SAMuonIO));
    } else {
      m_l2saio_pt->push_back(0.);
      m_l2saio_eta->push_back(0.);
      m_l2saio_phi->push_back(0.);
      m_l2saio_numberOfSPs0->push_back(-10);
      m_l2saio_numberOfSPs1->push_back(-10);
    }

    m_l2cbio_pass->push_back(isPassedCBIO);
    if ( matchedL2CBMuonIO ){
      ATH_MSG_INFO("matchedL2CBMuonIO pt/eta/phi: " << matchedL2CBMuonIO->pt() << "/" << matchedL2CBMuonIO->eta() << "/" << matchedL2CBMuonIO->phi());
      m_l2cbio_pt->push_back(matchedL2CBMuonIO->pt());
      m_l2cbio_eta->push_back(matchedL2CBMuonIO->eta());
      m_l2cbio_phi->push_back(matchedL2CBMuonIO->phi());
      m_h_pt_dR_CBIO_0->Fill(muon->pt()/1000., deltaR(muon->eta(), muon->phi(), matchedL2CBMuonIO->eta(), matchedL2CBMuonIO->phi()));
      m_h_pt_dR_CBIO_1->Fill(matchedL2CBMuonIO->pt(), deltaR(muon->eta(), muon->phi(), matchedL2CBMuonIO->eta(), matchedL2CBMuonIO->phi()));
    } else {
      m_l2cbio_pt->push_back(0.);
      m_l2cbio_eta->push_back(0.);
      m_l2cbio_phi->push_back(0.);
    }

    ATH_MSG_INFO("# of L2SAIO in RoI, nL2SAIOs: " << nL2SAIOs);
    ATH_MSG_INFO("# of high-pT L2SAIO in RoI, nL2SAIOs_highpt: " << nL2SAIOs_highpt);
    ATH_MSG_INFO("# of lowdr L2SAIO in RoI, nL2SAIOs_lowdr: " << nL2SAIOs_lowdr);
    m_ftf_size->push_back(nL2SAIOs);
    m_ftf_size1->push_back(nL2SAIOs_highpt);
    m_ftf_size2->push_back(nL2SAIOs_lowdr);

  } // offline muon loop

  ATH_MSG_INFO("m_muon_pt size = " << m_muon_pt->size());
  ATH_MSG_INFO("m_l1_pt size = " << m_l1_pt->size());
  ATH_MSG_INFO("m_l2sa_pt size = " << m_l2sa_pt->size());
  ATH_MSG_INFO("m_l2cb_pt size = " << m_l2cb_pt->size());
  ATH_MSG_INFO("m_l2saio_pt size = " << m_l2saio_pt->size());
  ATH_MSG_INFO("m_l2cbio_pt size = " << m_l2cbio_pt->size());
  ATH_MSG_INFO("m_l1_pass size = " << m_l1_pass->size());
  ATH_MSG_INFO("m_l2sa_pass size = " << m_l2sa_pass->size());
  ATH_MSG_INFO("m_l2cb_pass size = " << m_l2cb_pass->size());
  ATH_MSG_INFO("m_l2saio_pass size = " << m_l2saio_pass->size());
  ATH_MSG_INFO("m_l2cbio_pass size = " << m_l2cbio_pass->size());

  double muons_dR = 999999;
  bool L1pass = false;
  bool SApass = false;
  bool CBpass = false;
  bool SAIOpass = false;
  bool CBIOpass = false;
  bool L1sep = false;
  bool SAsep = false;
  bool CBsep = false;
  bool SAIOsep = false;
  bool CBIOsep = false;
  bool isBarrel = false;
  bool isEndcap = false;
  if (m_muon_pt->size() == 2){
    //muons_dR = deltaR(m_muon_eta->at(0),m_muon_phi->at(0),m_muon_eta->at(1),m_muon_phi->at(1));
    muons_dR = deltaR(m_muon_exteta->at(0),m_muon_extphi->at(0),m_muon_exteta->at(1),m_muon_extphi->at(1));
    double muons_IP_dR = deltaR(m_muon_eta->at(0),m_muon_phi->at(0),m_muon_eta->at(1),m_muon_phi->at(1));
    ATH_MSG_INFO("muons_dR: " << muons_dR << ", muons_IP_dR " << muons_IP_dR);

    m_h_di_ext_deltaR->Fill(muons_dR);
    m_h_di_IP_deltaR->Fill(muons_IP_dR);
    m_h_di_ext_IP_deltaR->Fill(muons_dR, muons_IP_dR);

    TLorentzVector mu1,mu2,jpsi;
    mu1.SetPtEtaPhiM( m_muon_pt->at(0), m_muon_eta->at(0), m_muon_phi->at(0), 105.6 );
    mu2.SetPtEtaPhiM( m_muon_pt->at(1), m_muon_eta->at(1), m_muon_phi->at(1), 105.6 );
    jpsi = mu1 + mu2;
    double jpsi_pt = jpsi.Pt();
    m_h_di_dR_pt->Fill(muons_dR, jpsi_pt);

    if ( abs(m_muon_eta->at(0)) < 1.05 && abs(m_muon_eta->at(1)) < 1.05 ){
      ATH_MSG_INFO("Barrel two muons");
      isBarrel = true;
    } else if ( abs(m_muon_eta->at(0)) > 1.05 && abs(m_muon_eta->at(1)) > 1.05 ){
      ATH_MSG_INFO("Endcap two muons");
      isEndcap = true;
    } else {
      ATH_MSG_INFO("Transition two muons?");
    }
  }

  //L1
  if (m_l1_pass->size() == 2){
    //L1 pass
    if (m_l1_pass->at(0) == 1 && m_l1_pass->at(1) == 1){
      L1pass = true;
    }
    //L1 sep
    if (abs(m_l1_eta->at(0) - m_l1_eta->at(1)) > ZERO_LIMIT || abs(m_l1_phi->at(0) - m_l1_phi->at(1)) > ZERO_LIMIT ){
      L1sep = true;
    }
  }

  //SA
  if (m_l2sa_pass->size() == 2){
    //SA pass
    if (m_l2sa_pass->at(0) == 1 && m_l2sa_pass->at(1) == 1){
      SApass = true;
    }
    //SA sep
    if (abs(m_l2sa_pt->at(0) - m_l2sa_pt->at(1)) > ZERO_LIMIT || abs(m_l2sa_eta->at(0) - m_l2sa_eta->at(1)) > ZERO_LIMIT || abs(m_l2sa_phi->at(0) - m_l2sa_phi->at(1)) > ZERO_LIMIT ){
      SAsep = true;
    }
  }

  //CB
  if (m_l2cb_pass->size() == 2){
    //CB pass
    if (m_l2cb_pass->at(0) == 1 && m_l2cb_pass->at(1) == 1){
      CBpass = true;
    }
    //CB sep
    if (abs(m_l2cb_pt->at(0) - m_l2cb_pt->at(1)) > ZERO_LIMIT || abs(m_l2cb_eta->at(0) - m_l2cb_eta->at(1)) > ZERO_LIMIT || abs(m_l2cb_phi->at(0) - m_l2cb_phi->at(1)) > ZERO_LIMIT ){
      CBsep = true;
    }
  }

  //SAIO
  if (m_l2saio_pass->size() == 2){
    //SAIO pass
    if (m_l2saio_pass->at(0) == 1 && m_l2saio_pass->at(1) == 1){
      SAIOpass = true;
    }
    //SAIO sep
    if (abs(m_l2saio_pt->at(0) - m_l2saio_pt->at(1)) > ZERO_LIMIT || abs(m_l2saio_eta->at(0) - m_l2saio_eta->at(1)) > ZERO_LIMIT || abs(m_l2saio_phi->at(0) - m_l2saio_phi->at(1)) > ZERO_LIMIT ){
      SAIOsep = true;
    }
  }

  //CBIO
  if (m_l2cb_pass->size() == 2){
    //CBIO pass
    if (m_l2cbio_pass->at(0) == 1 && m_l2cbio_pass->at(1) == 1){
      CBIOpass = true;
    }
    //CBIO sep
    if (abs(m_l2cbio_pt->at(0) - m_l2cbio_pt->at(1)) > ZERO_LIMIT || abs(m_l2cbio_eta->at(0) - m_l2cbio_eta->at(1)) > ZERO_LIMIT || abs(m_l2cbio_phi->at(0) - m_l2cbio_phi->at(1)) > ZERO_LIMIT ){
      CBIOsep = true;
    }
  }

  // two muosn result
  ATH_MSG_INFO("two muons result");
  ATH_MSG_INFO("isBarrel: " << isBarrel << ", isEndcap: " << isEndcap);
  ATH_MSG_INFO("L1pass/SApass/CBpass/SAIOpass/CBIOpass: " << L1pass << "/" << SApass << "/" << CBpass << "/" << SAIOpass << "/" << CBIOpass);
  ATH_MSG_INFO("L1sep/SAsep/CBsep/SAIOsep/CBIOsep: " << L1sep << "/" << SAsep << "/" << CBsep << "/" << SAIOsep << "/" << CBIOsep);
  ATH_MSG_INFO("size L1/SA/CB/SAIO/CBIO: " << m_l1_pt->size() << "/" << m_l2sa_pt->size() << "/" << m_l2cb_pt->size() << "/" << m_l2saio_pt->size() << "/" << m_l2cbio_pt->size());

  if (m_muon_pt->size()>0.)ATH_MSG_INFO("muon pt/eta/phi: "   << m_muon_pt->at(0) << "/" << m_muon_eta->at(0)     << "/" << m_muon_phi->at(0));
  if (m_muon_pt->size()>0.)ATH_MSG_INFO("muon pt/eta/phi: "   << m_muon_pt->at(1) << "/" << m_muon_eta->at(1)     << "/" << m_muon_phi->at(1));
  if (m_l1_pt->size()>0.)ATH_MSG_INFO("L1 pass/pt/eta/phi: " << m_l1_pass->at(0) << "/"  << m_l1_pt->at(0)     << "/" << m_l1_eta->at(0)     << "/" << m_l1_phi->at(0));
  if (m_l1_pt->size()>1.)ATH_MSG_INFO("L1 pass/pt/eta/phi: " << m_l1_pass->at(1) << "/"  << m_l1_pt->at(1)     << "/" << m_l1_eta->at(1)     << "/" << m_l1_phi->at(1));
  if (m_l2sa_pt->size()>0.)ATH_MSG_INFO("SA pass/pt/eta/phi: " << m_l2sa_pass->at(0) << "/"  << m_l2sa_pt->at(0)   << "/" << m_l2sa_eta->at(0)   << "/" << m_l2sa_phi->at(0));
  if (m_l2sa_pt->size()>1.)ATH_MSG_INFO("SA pass/pt/eta/phi: " << m_l2sa_pass->at(1) << "/"  << m_l2sa_pt->at(1)   << "/" << m_l2sa_eta->at(1)   << "/" << m_l2sa_phi->at(1));
  if (m_l2cb_pt->size()>0.)ATH_MSG_INFO("CB pass/pt/eta/phi: " << m_l2cb_pass->at(0) << "/"  << m_l2cb_pt->at(0)   << "/" << m_l2cb_eta->at(0)   << "/" << m_l2cb_phi->at(0));
  if (m_l2cb_pt->size()>1.)ATH_MSG_INFO("CB pass/pt/eta/phi: " << m_l2cb_pass->at(1) << "/"  << m_l2cb_pt->at(1)   << "/" << m_l2cb_eta->at(1)   << "/" << m_l2cb_phi->at(1));
  if (m_l2saio_pt->size()>0.)ATH_MSG_INFO("SAIO pass/pt/eta/phi: " << m_l2saio_pass->at(0) << "/"   << m_l2saio_pt->at(0) << "/" << m_l2saio_eta->at(0) << "/" << m_l2saio_phi->at(0));
  if (m_l2saio_pt->size()>1.)ATH_MSG_INFO("SAIO pass/pt/eta/phi: " << m_l2saio_pass->at(1) << "/"   << m_l2saio_pt->at(1) << "/" << m_l2saio_eta->at(1) << "/" << m_l2saio_phi->at(1));
  if (m_l2cbio_pt->size()>0.)ATH_MSG_INFO("CBIO pass/pt/eta/phi: " << m_l2cbio_pass->at(0) << "/"   << m_l2cbio_pt->at(0) << "/" << m_l2cbio_eta->at(0) << "/" << m_l2cbio_phi->at(0));
  if (m_l2cbio_pt->size()>1.)ATH_MSG_INFO("CBIO pass/pt/eta/phi: " << m_l2cbio_pass->at(1) << "/"   << m_l2cbio_pt->at(1) << "/" << m_l2cbio_eta->at(1) << "/" << m_l2cbio_phi->at(1));
  if (m_ftf_size->size()>0.)ATH_MSG_INFO("FTF size/size1: " << m_ftf_size->at(0) << "/" << m_ftf_size1->at(0) << "/" << m_ftf_size2->at(0));
  if (m_ftf_size->size()>1.)ATH_MSG_INFO("FTF size/size1: " << m_ftf_size->at(1) << "/" <<m_ftf_size1->at(1) << "/" << m_ftf_size2->at(1));


  //if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_muL1->Fill(m_muon_pt->at(0));

  // pt residual
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && abs(m_l2sa_pt->at(0)) > 0 ) m_h_pt_res_muSA->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2sa_pt->at(0))));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && abs(m_l2sa_pt->at(1)) > 0 ) m_h_pt_res_muSA->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2sa_pt->at(1))));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCB->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2cb_pt->at(0))));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCB->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2cb_pt->at(1))));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_res_muSAIO->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2saio_pt->at(0))));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 ) m_h_pt_res_muSAIO->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2saio_pt->at(1))));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCBIO->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2cbio_pt->at(0))));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 ) m_h_pt_res_muCBIO->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2cbio_pt->at(1))));

  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 ) m_h_pt_res_muSA_1->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2sa_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 ) m_h_pt_res_muSA_1->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2sa_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCB_1->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2cb_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCB_1->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2cb_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 ) m_h_pt_res_muSAIO_1->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2saio_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 ) m_h_pt_res_muSAIO_1->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2saio_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCBIO_1->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2cbio_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 ) m_h_pt_res_muCBIO_1->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2cbio_pt->at(1))));

  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(0) < 40 ) m_h_pt_res_muSA_2->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2sa_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 && m_muon_pt->at(1) < 40 ) m_h_pt_res_muSA_2->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2sa_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(0) < 40 ) m_h_pt_res_muCB_2->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2cb_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(1) < 40 ) m_h_pt_res_muCB_2->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2cb_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(0) < 40 ) m_h_pt_res_muSAIO_2->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2saio_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 && m_muon_pt->at(1) < 40 ) m_h_pt_res_muSAIO_2->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2saio_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(0) < 40 ) m_h_pt_res_muCBIO_2->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2cbio_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 && m_muon_pt->at(1) < 40 ) m_h_pt_res_muCBIO_2->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2cbio_pt->at(1))));

  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(0) < 40 && L1sep && SAsep ) m_h_pt_res_muSA_3->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2sa_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 && m_muon_pt->at(1) < 40 && L1sep && SAsep ) m_h_pt_res_muSA_3->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2sa_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(0) < 40 && L1sep && SAsep ) m_h_pt_res_muCB_3->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2cb_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(1) < 40 && L1sep && SAsep ) m_h_pt_res_muCB_3->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2cb_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(0) < 40 && L1sep && SAIOsep ) m_h_pt_res_muSAIO_3->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2saio_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 && m_muon_pt->at(1) < 40 && L1sep && SAIOsep ) m_h_pt_res_muSAIO_3->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2saio_pt->at(1))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(0) == 1 && m_muon_pt->at(0) < 40 && L1sep && CBIOsep ) m_h_pt_res_muCBIO_3->Fill(m_muon_pt->at(0), pTresidual(m_muon_pt->at(0),abs(m_l2cbio_pt->at(0))));
  if ( matchJpsi && isBarrel && muons_dR > 0.2 && m_l1_pass->at(1) == 1 && m_muon_pt->at(1) < 40 && L1sep && CBIOsep ) m_h_pt_res_muCBIO_3->Fill(m_muon_pt->at(1), pTresidual(m_muon_pt->at(1),abs(m_l2cbio_pt->at(1))));

  // # of SPs
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_numberOfSPs_muSA->Fill(m_muon_pt->at(0),m_l2sa_numberOfSPs0->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 ) m_h_pt_numberOfSPs_muSA->Fill(m_muon_pt->at(1),m_l2sa_numberOfSPs0->at(1));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2sa_pass->at(0) == 1 ) m_h_pt_numberOfSPs_muSA_1->Fill(m_muon_pt->at(0),m_l2sa_numberOfSPs0->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2sa_pass->at(1) == 1 ) m_h_pt_numberOfSPs_muSA_1->Fill(m_muon_pt->at(1),m_l2sa_numberOfSPs0->at(1));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2cb_pass->at(0) == 1 ) m_h_pt_numberOfSPs_muSA_2->Fill(m_muon_pt->at(0),m_l2sa_numberOfSPs0->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2cb_pass->at(1) == 1 ) m_h_pt_numberOfSPs_muSA_2->Fill(m_muon_pt->at(1),m_l2sa_numberOfSPs0->at(1));
  //if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCB->Fill(pTresidual(m_muon_pt->at(0),abs(m_l2cb_pt->at(0))));
  //if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCB->Fill(pTresidual(m_muon_pt->at(1),abs(m_l2cb_pt->at(1))));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1) {
    m_h_pt_numberOfSPs_muSAIO->Fill(m_muon_pt->at(0),m_l2saio_numberOfSPs0->at(0));
    ATH_MSG_INFO("m_l2saio_numberOfSPs0->at(0): " << m_l2saio_numberOfSPs0->at(0) << ", " << m_l2cbio_pt->at(0) << "/" << m_l2cbio_eta->at(0) << "/" << m_l2cbio_phi->at(0));
  }
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1) {
    m_h_pt_numberOfSPs_muSAIO->Fill(m_muon_pt->at(1),m_l2saio_numberOfSPs0->at(1));
    ATH_MSG_INFO("m_l2saio_numberOfSPs0->at(1): " << m_l2saio_numberOfSPs0->at(1) << ", " << m_l2cbio_pt->at(1) << "/" << m_l2cbio_eta->at(1) << "/" << m_l2cbio_phi->at(1));
  }
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2cbio_pass->at(0) == 1  ) m_h_pt_numberOfSPs_muSAIO_1->Fill(m_muon_pt->at(0),m_l2saio_numberOfSPs1->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2cbio_pass->at(1) == 1  ) m_h_pt_numberOfSPs_muSAIO_1->Fill(m_muon_pt->at(1),m_l2saio_numberOfSPs1->at(1));

  //if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_res_muCBIO->Fill(pTresidual(m_muon_pt->at(0),abs(m_l2cbio_pt->at(0))));
  //if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 ) m_h_pt_res_muCBIO->Fill(pTresidual(m_muon_pt->at(1),abs(m_l2cbio_pt->at(1))));

  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2cb_pass->at(0) == 1 ) m_h_pt_numberOfFTFs->Fill(m_muon_pt->at(0),  m_ftf_size->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2cb_pass->at(1) == 1 ) m_h_pt_numberOfFTFs->Fill(m_muon_pt->at(1),  m_ftf_size->at(1));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2cb_pass->at(0) == 1 ) m_h_pt_numberOfFTFs1->Fill(m_muon_pt->at(0), m_ftf_size1->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2cb_pass->at(1) == 1 ) m_h_pt_numberOfFTFs1->Fill(m_muon_pt->at(1), m_ftf_size1->at(1));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2cb_pass->at(0) == 1 ) m_h_pt_numberOfFTFs2->Fill(m_muon_pt->at(0), m_ftf_size2->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2cb_pass->at(1) == 1 ) m_h_pt_numberOfFTFs2->Fill(m_muon_pt->at(1), m_ftf_size2->at(1));


  // single muon eff
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 ) m_h_pt_muL1->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 ) m_h_pt_muL1->Fill(m_muon_pt->at(1));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2sa_pass->at(0) ==1 ) m_h_pt_muSA->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2sa_pass->at(1) ==1 ) m_h_pt_muSA->Fill(m_muon_pt->at(1));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2saio_pass->at(0) ==1 ) m_h_pt_muSAIO->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2saio_pass->at(1) ==1 ) m_h_pt_muSAIO->Fill(m_muon_pt->at(1));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2sa_pass->at(0) == 1 && m_l2cb_pass->at(0) ==1 ) m_h_pt_muCB->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2sa_pass->at(1) == 1 && m_l2cb_pass->at(0) ==1 ) m_h_pt_muCB->Fill(m_muon_pt->at(1));
  if ( matchJpsi && isBarrel && m_l1_pass->at(0) == 1 && m_l2cbio_pass->at(0) ==1 ) m_h_pt_muCBIO->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && m_l1_pass->at(1) == 1 && m_l2cbio_pass->at(1) ==1 ) m_h_pt_muCBIO->Fill(m_muon_pt->at(1));

  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(0) == 1 ) m_h_pt_muL1_1->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(1) == 1 ) m_h_pt_muL1_1->Fill(m_muon_pt->at(1));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(0) == 1 && m_l2sa_pass->at(0) ==1 ) m_h_pt_muSA_1->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(1) == 1 && m_l2sa_pass->at(1) ==1 ) m_h_pt_muSA_1->Fill(m_muon_pt->at(1));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(0) == 1 && m_l2saio_pass->at(0) ==1 ) m_h_pt_muSAIO_1->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(1) == 1 && m_l2saio_pass->at(1) ==1 ) m_h_pt_muSAIO_1->Fill(m_muon_pt->at(1));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(0) == 1 && m_l2sa_pass->at(0) == 1 && m_l2cb_pass->at(0) ==1 ) m_h_pt_muCB_1->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(1) == 1 && m_l2sa_pass->at(1) == 1 && m_l2cb_pass->at(0) ==1 ) m_h_pt_muCB_1->Fill(m_muon_pt->at(1));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(0) == 1 && m_l2cbio_pass->at(0) ==1 ) m_h_pt_muCBIO_1->Fill(m_muon_pt->at(0));
  if ( matchJpsi && isBarrel && muons_dR > 0.1 && m_l1_pass->at(1) == 1 && m_l2cbio_pass->at(1) ==1 ) m_h_pt_muCBIO_1->Fill(m_muon_pt->at(1));

  if ( matchJpsi && isBarrel && m_muon_pt->at(0) > 10. && m_muon_pt->at(1) > 10. ){
    bool L1pass0   = ( m_l1_pass->at(0)     == 1 );
    bool L1pass1   = ( m_l1_pass->at(1)     == 1 );
    bool SApass0   = ( m_l2sa_pass->at(0)   == 1 );
    bool SApass1   = ( m_l2sa_pass->at(1)   == 1 );
    bool CBpass0   = ( m_l2cb_pass->at(0)   == 1 );
    bool CBpass1   = ( m_l2cb_pass->at(1)   == 1 );
    bool SAIOpass0 = ( m_l2saio_pass->at(0) == 1 );
    bool SAIOpass1 = ( m_l2saio_pass->at(1) == 1 );
    bool CBIOpass0 = ( m_l2cbio_pass->at(0) == 1 );
    bool CBIOpass1 = ( m_l2cbio_pass->at(1) == 1 );

    int SPIOnSPs0 = m_l2saio_numberOfSPs0->at(0);
    int SPIOnSPs1 = m_l2saio_numberOfSPs0->at(1);
    ATH_MSG_INFO("SPIOnSPs0/SPIOnSPs1: " << SPIOnSPs0 << "/" << SPIOnSPs1);

    if ( L1pass0 && L1sep ) m_h_dR_2muL1_sep->Fill(muons_dR);
    if ( L1pass1 && L1sep ) m_h_dR_2muL1_sep->Fill(muons_dR);
    if ( L1pass0 && L1sep && SApass0 && SAsep ) m_h_dR_2muSA_sep->Fill(muons_dR);
    if ( L1pass1 && L1sep && SApass1 && SAsep ) m_h_dR_2muSA_sep->Fill(muons_dR);
    if ( L1pass0 && L1sep && SApass0 && SAsep && CBpass0 && CBsep ) m_h_dR_2muCB_sep->Fill(muons_dR);
    if ( L1pass1 && L1sep && SApass1 && SAsep && CBpass1 && CBsep ) m_h_dR_2muCB_sep->Fill(muons_dR);
    if ( L1pass0 && L1sep && SAIOpass0 && SAIOsep ) m_h_dR_2muSAIO_sep->Fill(muons_dR);
    if ( L1pass1 && L1sep && SAIOpass1 && SAIOsep ) m_h_dR_2muSAIO_sep->Fill(muons_dR);
    if ( L1pass0 && L1sep && (SAIOpass0 || (CBIOpass0 && SPIOnSPs0>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep->Fill(muons_dR);
    if ( L1pass1 && L1sep && (SAIOpass1 || (CBIOpass1 && SPIOnSPs1>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep->Fill(muons_dR);

    if ( L1pass0 ) m_h_dR_2muL1_sep1->Fill(muons_dR);
    if ( L1pass1 ) m_h_dR_2muL1_sep1->Fill(muons_dR);
    if ( L1pass0 && SApass0 && SAsep) m_h_dR_2muSA_sep1->Fill(muons_dR);
    if ( L1pass1 && SApass1 && SAsep) m_h_dR_2muSA_sep1->Fill(muons_dR);
    if ( L1pass0 && SApass0 && SAsep && CBpass0 && CBsep) m_h_dR_2muCB_sep1->Fill(muons_dR);
    if ( L1pass1 && SApass1 && SAsep && CBpass1 && CBsep) m_h_dR_2muCB_sep1->Fill(muons_dR);
    if ( L1pass0 && SApass0 && CBpass0 && SAIOpass0 && SAIOsep) m_h_dR_2muSAIO_sep1->Fill(muons_dR);
    if ( L1pass1 && SApass1 && CBpass1 && SAIOpass1 && SAIOsep) m_h_dR_2muSAIO_sep1->Fill(muons_dR);
    if ( L1pass0 && SApass0 && CBpass0 && (SAIOpass0 || (CBIOpass0 && SPIOnSPs0>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep1->Fill(muons_dR);
    if ( L1pass1 && SApass1 && CBpass1 && (SAIOpass1 || (CBIOpass1 && SPIOnSPs1>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep1->Fill(muons_dR);

    if ( L1pass0 && !L1sep) m_h_dR_2muL1_sep2->Fill(muons_dR);
    if ( L1pass1 && !L1sep) m_h_dR_2muL1_sep2->Fill(muons_dR);
    if ( L1pass0 && !L1sep && SApass0 && SAsep) m_h_dR_2muSA_sep2->Fill(muons_dR);
    if ( L1pass1 && !L1sep && SApass1 && SAsep) m_h_dR_2muSA_sep2->Fill(muons_dR);
    if ( L1pass0 && !L1sep && SApass0 && SAsep && CBpass0 && CBsep) m_h_dR_2muCB_sep2->Fill(muons_dR);
    if ( L1pass1 && !L1sep && SApass1 && SAsep && CBpass1 && CBsep) m_h_dR_2muCB_sep2->Fill(muons_dR);
    if ( L1pass0 && !L1sep && SAIOpass0 && SAIOsep) m_h_dR_2muSAIO_sep2->Fill(muons_dR);
    if ( L1pass1 && !L1sep && SAIOpass1 && SAIOsep) m_h_dR_2muSAIO_sep2->Fill(muons_dR);
    if ( L1pass0 && !L1sep && (SAIOpass0 || (CBIOpass0 && SPIOnSPs0>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep2->Fill(muons_dR);
    if ( L1pass1 && !L1sep && (SAIOpass1 || (CBIOpass1 && SPIOnSPs1>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep2->Fill(muons_dR);

    if ( L1pass0 && L1sep ) m_h_dR_2muL1_sep3->Fill(muons_dR);
    if ( L1pass1 && L1sep ) m_h_dR_2muL1_sep3->Fill(muons_dR);
    if ( L1pass0 && L1sep && SApass0 ) m_h_dR_2muSA_sep3->Fill(muons_dR);
    if ( L1pass1 && L1sep && SApass1 ) m_h_dR_2muSA_sep3->Fill(muons_dR);
    if ( L1pass0 && L1sep && SApass0 && CBpass0 && CBsep ) m_h_dR_2muCB_sep3->Fill(muons_dR);
    if ( L1pass1 && L1sep && SApass1 && CBpass1 && CBsep ) m_h_dR_2muCB_sep3->Fill(muons_dR);
    if ( L1pass0 && L1sep && SAIOpass0 && SAIOsep) m_h_dR_2muSAIO_sep3->Fill(muons_dR);
    if ( L1pass1 && L1sep && SAIOpass1 && SAIOsep) m_h_dR_2muSAIO_sep3->Fill(muons_dR);
    if ( L1pass0 && L1sep && (SAIOpass0 || (CBIOpass0 && SPIOnSPs0>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep3->Fill(muons_dR);
    if ( L1pass1 && L1sep && (SAIOpass1 || (CBIOpass1 && SPIOnSPs1>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep3->Fill(muons_dR);

    if ( L1pass0 && L1sep ) m_h_dR_2muL1_sep4->Fill(muons_dR);
    if ( L1pass1 && L1sep ) m_h_dR_2muL1_sep4->Fill(muons_dR);
    if ( L1pass0 && L1sep && SApass0 && !SAsep ) m_h_dR_2muSA_sep4->Fill(muons_dR);
    if ( L1pass1 && L1sep && SApass1 && !SAsep ) m_h_dR_2muSA_sep4->Fill(muons_dR);
    if ( L1pass0 && L1sep && SApass0 && CBpass0 && !SAsep && !CBsep ) m_h_dR_2muCB_sep4->Fill(muons_dR);
    if ( L1pass1 && L1sep && SApass1 && CBpass1 && !SAsep && !CBsep ) m_h_dR_2muCB_sep4->Fill(muons_dR);
    if ( L1pass0 && L1sep && SAIOpass0 && SAIOsep) m_h_dR_2muSAIO_sep4->Fill(muons_dR);
    if ( L1pass1 && L1sep && SAIOpass1 && SAIOsep) m_h_dR_2muSAIO_sep4->Fill(muons_dR);
    if ( L1pass0 && L1sep && SApass0 && CBpass0 && !SAsep && !CBsep && (SAIOpass0 ||  (CBIOpass0 && SPIOnSPs0>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep4->Fill(muons_dR);
    if ( L1pass1 && L1sep && SApass1 && CBpass1 && !SAsep && !CBsep && (SAIOpass1 ||  (CBIOpass1 && SPIOnSPs1>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep4->Fill(muons_dR);

    if ( L1pass0 ) m_h_dR_2muL1_sep5->Fill(muons_dR);
    if ( L1pass1 ) m_h_dR_2muL1_sep5->Fill(muons_dR);
    if ( L1pass0 && SApass0 && !SAsep ) m_h_dR_2muSA_sep5->Fill(muons_dR);
    if ( L1pass1 && SApass1 && !SAsep ) m_h_dR_2muSA_sep5->Fill(muons_dR);
    if ( L1pass0 && SApass0 && CBpass0 && !SAsep && !CBsep ) m_h_dR_2muCB_sep5->Fill(muons_dR);
    if ( L1pass1 && SApass1 && CBpass1 && !SAsep && !CBsep ) m_h_dR_2muCB_sep5->Fill(muons_dR);
    if ( L1pass0 && SAIOpass0 && SAIOsep) m_h_dR_2muSAIO_sep5->Fill(muons_dR);
    if ( L1pass1 && SAIOpass1 && SAIOsep) m_h_dR_2muSAIO_sep5->Fill(muons_dR);
    if ( L1pass0 && SApass0 && CBpass0 && !SAsep && !CBsep && (SAIOpass0 || (CBIOpass0 && SPIOnSPs0>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep5->Fill(muons_dR);
    if ( L1pass1 && SApass1 && CBpass1 && !SAsep && !CBsep && (SAIOpass1 || (CBIOpass1 && SPIOnSPs1>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep5->Fill(muons_dR);

    if ( L1pass0 ) m_h_dR_2muL1_sep6->Fill(muons_dR);
    if ( L1pass1 ) m_h_dR_2muL1_sep6->Fill(muons_dR);
    if ( L1pass0 && SApass0 ) m_h_dR_2muSA_sep6->Fill(muons_dR);
    if ( L1pass1 && SApass1 ) m_h_dR_2muSA_sep6->Fill(muons_dR);
    if ( L1pass0 && SApass0 && CBpass0 && CBsep ) m_h_dR_2muCB_sep6->Fill(muons_dR);
    if ( L1pass1 && SApass1 && CBpass1 && CBsep ) m_h_dR_2muCB_sep6->Fill(muons_dR);
    if ( L1pass0 && SAIOpass0 && SAIOsep) m_h_dR_2muSAIO_sep6->Fill(muons_dR);
    if ( L1pass1 && SAIOpass1 && SAIOsep) m_h_dR_2muSAIO_sep6->Fill(muons_dR);
    if ( L1pass0 && (SAIOpass0 || (CBIOpass0 && SPIOnSPs0>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep6->Fill(muons_dR);
    if ( L1pass1 && (SAIOpass1 || (CBIOpass1 && SPIOnSPs1>0.) ) && (SAIOsep || CBIOsep) ) m_h_dR_2muCBIO_sep6->Fill(muons_dR);
  }


  if (matchJpsi){
    if ( m_muon_pt->at(0) > 6. || m_muon_pt->at(0) < 20. ){
      if ( m_l1_pass->at(0) == 1 && m_l2sa_pass->at(0) == -1 ){
        ATH_MSG_INFO("Yuhi0");
      }
    }
    if ( m_muon_pt->at(1) > 6. || m_muon_pt->at(1) < 20. ){
      if ( m_l1_pass->at(1) == 1 && m_l2sa_pass->at(1) == -1 ){
        ATH_MSG_INFO("Yuhi1");
      }
    }
  }



  //// L2CB info dump
  //ATH_MSG_INFO("L2CombinedMuonContainer Key: " << cbKey);
  ////m_nMuons = l2samuons->size();
  //ATH_MSG_INFO(cbKey << "size = " << l2cbmuons->size());
  //for ( const auto& l2cbmuon : *l2cbmuons ) {
  //  ATH_MSG_INFO("L2CB pt/eta/phi=" << (l2cbmuon->pt())/1000. << "/" << l2cbmuon->eta() << "/" << l2cbmuon->phi());
  //  const xAOD::L2StandAloneMuon* L2SAMuon = NULL;
  //  const ElementLink< xAOD::L2StandAloneMuonContainer >& Link  = l2cbmuon->muSATrackLink();
  //  if (Link.isValid()) L2SAMuon = *Link;
  //  if (L2SAMuon == NULL) {
  //    ATH_MSG_INFO("No muSATrackLink" );   
  //  } else {
  //    ATH_MSG_INFO("muSATrackLink = " << L2SAMuon->roiNumber() );
  //  }
  //  const xAOD::TrackParticle* id = NULL;
  //  const ElementLink< xAOD::TrackParticleContainer >& idLink  = l2cbmuon->idTrackLink();
  //  if (idLink.isValid()) id = *idLink;
  //  if (id == NULL) {
  //    ATH_MSG_INFO("No idTrackLink" );   
  //  } else {
  //    ATH_MSG_INFO("idTrackLink = " << id->pt() );
  //  }
  //}

  //// L2CBIO info dump
  //ATH_MSG_INFO("L2StandAloneMuonContainer Key for IOmode: " << cbIOKey);
  ////m_nMuons = l2cbmuons->size();
  //ATH_MSG_INFO(cbIOKey << "size = " << l2cbmuonIOs->size());
  //for ( const auto& l2cbmuonIO : *l2cbmuonIOs) {
  //  ATH_MSG_INFO("L2cbIO pt/eta/phi=" << l2cbmuonIO->pt() << "/" << l2cbmuonIO->eta() << "/" << l2cbmuonIO->phi());
  //  const xAOD::L2StandAloneMuon* L2SAMuon = NULL;
  //  const ElementLink< xAOD::L2StandAloneMuonContainer >& Link  = l2cbmuonIO->muSATrackLink();
  //  if (Link.isValid()) L2SAMuon = *Link;
  //  if (L2SAMuon == NULL) {
  //    ATH_MSG_INFO("No muSATrackLink" );   
  //  } else {
  //    ATH_MSG_INFO("muSATrackLink = " << L2SAMuon->roiNumber() );
  //  }
  //  const xAOD::TrackParticle* id = NULL;
  //  const ElementLink< xAOD::TrackParticleContainer >& idLink  = l2cbmuonIO->idTrackLink();
  //  if (idLink.isValid()) id = *idLink;
  //  if (id == NULL) {
  //    ATH_MSG_INFO("No idTrackLink" );   
  //  } else {
  //    ATH_MSG_INFO("idTrackLink = " << id->pt() );
  //  }
  //}


  // L1 info dump
  ATH_MSG_INFO(L1Key << " size = " << rois->size());
  for( const auto& roi : *rois ) {
    int roiThr       = roi->getThrNumber();
    double roiEta    = roi->eta();
    double roiPhi  = roi->phi();
    const int roiNum = roi->getRoI();
    const int roiPt  = roi->thrValue();
    ATH_MSG_INFO("L1: roiNum/roiThr/roiPt/roiEta/roiPhi = " << roiNum << "/" << roiThr << "/" << roiPt << "/" << roiEta << "/" << roiPhi);
  }

  // L2SA info dump
  ATH_MSG_INFO("L2StandAloneMuonContainer Key: " << saKey);
  //m_nMuons = l2samuons->size();
  ATH_MSG_INFO(saKey << "size = " << l2samuons->size());
  for ( const auto& l2sa : *l2samuons ) {
    ATH_MSG_INFO("L2SA roiNum/pt/eta/phi/roiEta/roiPhi=" << l2sa->roiNumber() << "/" << l2sa->pt() << "/" << l2sa->eta() << "/" << l2sa->phi() << "/" << l2sa->roiEta() << "/" << l2sa->roiPhi());
  }


  // L2SAIO info dump
  ATH_MSG_INFO("L2StandAloneMuonContainer Key for IOmode: " << saIOKey);
  //m_nMuons = l2samuons->size();
  ATH_MSG_INFO(saIOKey << "size = " << l2samuonIOs->size());
  for ( const auto& l2samuonIO : *l2samuonIOs ) {
    ATH_MSG_INFO("L2SAIO pt/eta/phi/roiEta/roiPhi=" << l2samuonIO->pt() << "/" << l2samuonIO->eta() << "/" << l2samuonIO->phi() << "/" << l2samuonIO->roiEta() << "/" << l2samuonIO->roiPhi());
  }

  //FillRateHist(mus, rois).setChecked();
  //StatusCode scFillRateHist = FillRateHist(rois);
  //if(StatusCode::SUCCESS!=scFillRateHist) {
  //  ATH_MSG_WARNING("Could not FillRateHist(rois)");
  //}

  // retrieve muon rois
  //const xAOD::TrackParticleContainer* ftfs = 0;
  //ATH_CHECK( evtStore()->retrieve( ftfs, "HLT_xAOD__TrackParticleContainer_InDetTrigTrackingxAODCnv_Muon_FTF" ) );
  //ATH_CHECK( evtStore()->retrieve( ftfs, "HLT_xAOD__TrackParticleContainer_InDetTrigTrackingxAODCnv_Muon_FTKRefit" ) );
  // do tag and probe

  m_tree->Fill();
  ATH_MSG_INFO("m_tree->GetEntries(): " << m_tree->GetEntries());
  //m_tree->Print();

  cout << "end m_et.filltree()" << endl;

  m_isFirstEvent = false;

  double all_dimuMass = 0;
  for ( unsigned int i = 0; i < muons->size(); i++ ){
    const xAOD::Muon* muon1 = muons->at(i);
    for ( unsigned int j = 0; j < muons->size(); j++ ){
      if ( i == j ) continue;
      const xAOD::Muon* muon2 = muons->at(j);
      lvmu1.SetPtEtaPhiM( muon1->pt(), muon1->eta(), muon1->phi(), 105.6 );
      lvmu2.SetPtEtaPhiM( muon2->pt(), muon2->eta(), muon2->phi(), 105.6 );
      lvdimu = lvmu1 + lvmu2;
      all_dimuMass = lvdimu.M();
      ATH_MSG_INFO("all_dimuMass: " << all_dimuMass << ", " << muon1->pt() << "/" << muon2->pt());
      m_h_dimumass->Fill(all_dimuMass/1000.);
    }
  }

  double cbio_dimuMass = 0;
  TLorentzVector lv_l2cb1, lv_l2cb2, lv_l2cb_dimu;
  if ( m_l2cbio_pt->size() == 2){
    lv_l2cb1.SetPtEtaPhiM( m_l2cbio_pt->at(0)*1000., m_l2cbio_eta->at(0), m_l2cbio_phi->at(0), 105.6 );
    lv_l2cb2.SetPtEtaPhiM( m_l2cbio_pt->at(1)*1000., m_l2cbio_eta->at(1), m_l2cbio_phi->at(1), 105.6 );
    lv_l2cb_dimu = lv_l2cb1 + lv_l2cb2;
    cbio_dimuMass = lv_l2cb_dimu.M();
    m_h_l2cbio_dimuMass->Fill(cbio_dimuMass/1000.);
    ATH_MSG_INFO("cbio_dimuMass: " << cbio_dimuMass << ", " << m_l2cbio_pt->at(0) << "/" << m_l2cbio_pt->at(0));
  }

  return StatusCode::SUCCESS;
}

//StatusCode CalcEffAlg::checkFtk() {
//
//  ATH_MSG_INFO("");
//  ATH_MSG_INFO("checkFtk()");
//
//  const xAOD::EventInfo* eventInfo = 0;
//  StatusCode sc = evtStore()->retrieve(eventInfo);
//  if(StatusCode::SUCCESS!=sc || !eventInfo) {
//    ATH_MSG_WARNING("Could not retrieve EventInfo in checkFtk()");
//    return StatusCode::SUCCESS;
//  }
//
//  m_eventNumber = eventInfo->runNumber();
//  m_runNumber = eventInfo->eventNumber();
//  m_lumiBlock = eventInfo-> lumiBlock();
//  m_averageInteractionsPerCrossing =  eventInfo -> averageInteractionsPerCrossing();
//
//  ATH_MSG_INFO("=========== Run = " << m_runNumber << " : Event = " << m_eventNumber << " : mu = " << m_averageInteractionsPerCrossing );
//
//  m_muon_author->clear();
//  m_muon_type->clear();
//  m_muon_phi->clear();
//  m_muon_charge->clear();
//  m_muon_eta->clear();
//  m_muon_pt->clear();
//  m_ftk_phi->clear();
//  m_ftk_eta->clear();
//  m_ftk_pt->clear();
//  m_ftk_charge->clear();
//  m_ftk_d0->clear();
//  m_ftk_z0->clear();
//
//  const xAOD::L2StandAloneMuonContainer* l2saMuons = 0;
//  int iL2MuonSA = 0;
//  ATH_CHECK( evtStore() -> retrieve( l2saMuons, "HLT_xAOD__L2StandAloneMuonContainer_MuonL2SAInfo" ) );
//  int nL2MuonSA = l2saMuons->size();
//  for( auto l2saMuon : *l2saMuons ){ //// brief description
//    iL2MuonSA += 1;
//    static SG::AuxElement::Accessor< vector<int> > accessor_extflag( "extFlag" );
//    static SG::AuxElement::Accessor< int > accessor_roadalgo( "roadAlgo" );
//    static SG::AuxElement::Accessor< double > accessor_ptftk( "ptFtk" );
//    static SG::AuxElement::Accessor< double > accessor_etaftk( "etaFtk" );
//    static SG::AuxElement::Accessor< double > accessor_phiftk( "phiFtk" );
//    ATH_MSG_INFO("isAvailable:" << accessor_roadalgo.isAvailable((*l2saMuon)) << ", " << accessor_extflag.isAvailable((*l2saMuon)) << ", " << accessor_etaftk.isAvailable((*l2saMuon))  << ", " << accessor_ptftk.isAvailable((*l2saMuon)) );
//    if (accessor_extflag.isAvailable(( *l2saMuon ))){
//      ATH_MSG_INFO("Flag/roadAlgo/pt/ = " << (accessor_extflag(*l2saMuon))[0] << "/" << (accessor_roadalgo(*l2saMuon)) << "/" << (accessor_ptftk(*l2saMuon)) );
//
//      ATH_MSG_INFO("size:" << (accessor_extflag(*l2saMuon)).size() << ", " << (accessor_extflag(*l2saMuon))[0]  );
//      ATH_MSG_INFO("roadAlgo:" << accessor_roadalgo.isAvailable((*l2saMuon)) << ", " << accessor_roadalgo(*l2saMuon) );
//
//      if (accessor_ptftk(*l2saMuon) > 0.){
//        // inner
//        m_h_extFlag[0]->Fill( (accessor_extflag(*l2saMuon))[0] );
//        m_h_ptFtk[0]->Fill( accessor_ptftk(*l2saMuon)/1000. );
//        if ((accessor_extflag(*l2saMuon))[0] == 0 || (accessor_extflag(*l2saMuon))[0] == 1 ||(accessor_extflag(*l2saMuon))[0] == 2){
//          m_h_extFlag[1]->Fill( (accessor_extflag(*l2saMuon))[0] );
//          m_h_ptFtk[1]->Fill( accessor_ptftk(*l2saMuon)/1000. );
//        }
//        // middle
//        m_h_extFlag[2]->Fill( (accessor_extflag(*l2saMuon))[1] );
//        m_h_ptFtk[2]->Fill( accessor_ptftk(*l2saMuon)/1000. );
//        if ((accessor_extflag(*l2saMuon))[1] == 0 || (accessor_extflag(*l2saMuon))[1] == 1 ||(accessor_extflag(*l2saMuon))[1] == 2){
//          m_h_extFlag[3]->Fill( (accessor_extflag(*l2saMuon))[1] );
//          m_h_ptFtk[3]->Fill( accessor_ptftk(*l2saMuon)/1000. );
//        }
//        // outer
//        m_h_extFlag[4]->Fill( (accessor_extflag(*l2saMuon))[2] );
//        m_h_ptFtk[4]->Fill( accessor_ptftk(*l2saMuon)/1000. );
//        if ((accessor_extflag(*l2saMuon))[2] == 0 || (accessor_extflag(*l2saMuon))[2] == 1 ||(accessor_extflag(*l2saMuon))[2] == 2){
//          m_h_extFlag[5]->Fill( (accessor_extflag(*l2saMuon))[2] );
//          m_h_ptFtk[5]->Fill( accessor_ptftk(*l2saMuon)/1000. );
//        }
//      }
//      //m_h_extFlag[0]->Fill( (accessor_extflag(*l2saMuon))[0] );
//      //m_h_ptFtk[0]->Fill( accessor_ptftk(*l2saMuon)/1000. );
//      //m_h_etaFtk[0]->Fill( accessor_etaftk(*l2saMuon) );
//      //m_h_phiFtk[0]->Fill( accessor_phiftk(*l2saMuon) );
//    }
//
//  } 
//
//  // FTK
//  const xAOD::TrackParticleContainer* idtracks = 0;
//  //ATH_CHECK( evtStore()->retrieve( idtracks, "HLT_xAOD__TrackParticleContainer_InDetTrigTrackingxAODCnv_Muon_FTKRefit" ) );
//  ATH_CHECK( evtStore()->retrieve( idtracks, "FTK_TrackParticleContainerRefit" ) );
//  if(idtracks) {
//    cout << "FTK size=" << idtracks->size() << endl;
//    m_nFtks = idtracks->size();
//    for (xAOD::TrackParticleContainer::const_iterator trkit = idtracks->begin(); trkit != idtracks->end(); ++trkit) {
//      if ( ((*trkit)->pt()/1000.) > 2 ){
//        cout << "High FTK pt/eta/phi=" << (*trkit)->pt() << "/" << (*trkit)->eta() << "/" << (*trkit)->phi() << endl;
//      } else {
//        cout << "FTK pt/eta/phi=" << (*trkit)->pt() << "/" << (*trkit)->eta() << "/" << (*trkit)->phi() << endl;
//      }
//      m_ftk_eta->push_back((*trkit)->eta());
//      m_ftk_phi->push_back((*trkit)->phi());
//      m_ftk_pt->push_back((*trkit)->pt());
//      m_ftk_charge->push_back((*trkit)->charge());
//      m_ftk_d0->push_back((*trkit)->d0());
//      m_ftk_z0->push_back((*trkit)->z0());
//    }
//  }
//
//  const xAOD::TrackParticleContainer* idtrack_ftfs = 0;
//  ATH_CHECK( evtStore()->retrieve( idtrack_ftfs, "HLT_xAOD__TrackParticleContainer_InDetTrigTrackingxAODCnv_Muon_FTKRefit" ) );
//  if(idtrack_ftfs) {
//    cout << "HLT FTK size=" << idtrack_ftfs->size() << endl;
//    for (xAOD::TrackParticleContainer::const_iterator trkit = idtrack_ftfs->begin(); trkit != idtrack_ftfs->end(); ++trkit) {
//      if ( ((*trkit)->pt()/1000.) > 2 ){
//        cout << "High HLT FTK pt/eta/phi=" << (*trkit)->pt() << "/" << (*trkit)->eta() << "/" << (*trkit)->phi() << endl;
//      } else {
//        cout << "HLT FTK pt/eta/phi=" << (*trkit)->pt() << "/" << (*trkit)->eta() << "/" << (*trkit)->phi() << endl;
//      }
//    }
//  }
//
//
//  // Offline
//  const xAOD::MuonContainer* muons = 0;
//  ATH_CHECK( evtStore()->retrieve( muons, "Muons" ) );
//  xAOD::MuonContainer::const_iterator mu_itr = muons->begin();
//  xAOD::MuonContainer::const_iterator mu_end  = muons->end();
//  m_nMuons = muons->size();
//  cout << "Muon size=" << muons->size() << endl;
//  for ( ; mu_itr!=mu_end; ++mu_itr ) {
//    const double muPt     = (*mu_itr)->pt();
//    cout << "Offline pt/eta/phi=" << ((*mu_itr)->pt())/1000. << "/" << (*mu_itr)->eta() << "/" << (*mu_itr)->phi() << endl;
//    m_muon_eta->push_back((*mu_itr)->eta());
//    m_muon_phi->push_back((*mu_itr)->phi());
//    m_muon_pt->push_back((*mu_itr)->pt());
//    m_muon_charge->push_back((*mu_itr)->charge());
//    m_muon_author->push_back((*mu_itr)->author());
//    m_muon_type->push_back((*mu_itr)->muonType());
//  }
//
//  const xAOD::MuonRoIContainer* rois = 0;
//  ATH_CHECK( evtStore()->retrieve( rois, "LVL1MuonRoIs" ) );
//  cout << "RoI size=" << rois->size() << endl;
//  for(xAOD::MuonRoIContainer::const_iterator roi = rois->begin(); roi != rois->end(); ++roi) {
//    int roiThr       = (*roi)->getThrNumber();
//    double roiEta    = (*roi)->eta();
//    double roiPhi  = (*roi)->phi();
//    const int roiNum = (*roi)->getRoI();
//    const int roiPt  = (*roi)->thrValue();
//    cout << "L1: roiNum/roiThr/roiPt/roiEta/roiPhi = " << roiNum << "/" << roiThr << "/" << roiPt << "/" << roiEta << "/" << roiPhi << endl;
//  }
//
//  return StatusCode::SUCCESS;
//}

//StatusCode CalcEffAlg::FillRateHist(const xAOD::MuonContainer* muons, const xAOD::MuonRoIContainer* rois ) {
//
//  ATH_MSG_INFO("");
//  ATH_MSG_INFO("For Loop in FillRateHist()");
//
//  const xAOD::EventInfo* eventInfo = 0;
//  StatusCode sc = evtStore()->retrieve(eventInfo);
//  if(StatusCode::SUCCESS!=sc || !eventInfo) {
//    ATH_MSG_WARNING("Could not retrieve EventInfo in FillRateHist()");
//    return StatusCode::SUCCESS;
//  }
//
//  uint32_t runNumber = eventInfo->runNumber();
//  unsigned long long eventNumber = eventInfo->eventNumber();
//
//  for( unsigned int iChain = 0; iChain < m_trigL1.size(); ++iChain){
//    // Set HLT TE names
//    std::string trigSAName = "";
//    //getSATEName( m_trigHLT[iChain], trigSAName );
//    std::string trigCBName = "";
//    //getCBTEName( m_trigHLT[iChain], trigCBName );
//
//    ATH_MSG_INFO("");
//    ATH_MSG_INFO("[iChain = " << iChain
//        << ", trigEvent = " << m_trigEvent[iChain]
//        << ", trigHLT = " << m_trigHLT[iChain]
//        << ", trigL1 = " << m_trigL1[iChain]
//        << ", trigSAName = " << trigSAName
//        << ", trigCBName = " << trigCBName
//        << ", trigThreshold = " << m_trigThreshold[iChain]
//        << "]");
//
//    //bool EventTriggerPassed = m_trigDecTool->isPassed(m_trigEvent[iChain],TrigDefs::Physics);
//    //ATH_MSG_INFO("PreScale: " << m_trigDecTool -> getPrescale(m_trigEvent[iChain]));
//    //ATH_MSG_INFO("eventAccepted: " << m_trigDecTool->isPassed(m_trigEvent[iChain],TrigDefs::eventAccepted));
//    //ATH_MSG_INFO("passedThrough: " << m_trigDecTool->isPassed(m_trigEvent[iChain],TrigDefs::passedThrough));
//    //ATH_MSG_INFO("Physics | passedThrough: " << m_trigDecTool->isPassed(m_trigEvent[iChain],TrigDefs::Physics | TrigDefs::passedThrough));
//    //ATH_MSG_INFO("requireDecision: " << m_trigDecTool->isPassed(m_trigEvent[iChain],TrigDefs::requireDecision));
//    //ATH_MSG_INFO("allowResurrectedDecision: " << m_trigDecTool->isPassed(m_trigEvent[iChain],TrigDefs::allowResurrectedDecision));
//    //ATH_MSG_INFO("enforceLogicalFlow: " << m_trigDecTool->isPassed(m_trigEvent[iChain],TrigDefs::enforceLogicalFlow));
//    //ATH_MSG_INFO("allowResurrectedDecision | requireDecision | enforceLogicalFlow: " << m_trigDecTool->isPassed(m_trigEvent[iChain],TrigDefs::allowResurrectedDecision | TrigDefs::requireDecision | TrigDefs::enforceLogicalFlow));
//    bool EventTriggerPassed = false;
//    if (!EventTriggerPassed){
//      ATH_MSG_INFO("Event Trigger is not passed: " << m_trigEvent[iChain]);
//      //ATH_MSG_INFO("Check eventNumber: " << - eventNumber);
//      //m_h_countEvent[iChain] -> Fill(- eventNumber);
//      continue;
//    } else {
//      ATH_MSG_INFO("Event Trigger is passed: " << m_trigEvent[iChain]);
//      //ATH_MSG_INFO("Check eventNumber: " << + eventNumber);
//      m_h_countEvent[iChain] -> Fill(+ eventNumber);
//    }
//
//
//    //L1
//    xAOD::MuonRoIContainer::const_iterator roi_itr = rois->begin();
//    xAOD::MuonRoIContainer::const_iterator roi_end = rois->end();
//    for(; roi_itr!=roi_end; ++roi_itr){
//      int roiThr       = (*roi_itr)->getThrNumber();
//      double roiEta    = (*roi_itr)->eta();
//      const int roiNum = (*roi_itr)->getRoI();
//      const int roiPt  = (*roi_itr)->thrValue();
//      ATH_MSG_INFO("L1: roiNum = " << roiNum
//          << ", roiThr = " << roiThr
//          << ", roiPt = " << roiPt
//          << ", roiEta = " << roiEta);
//      // Check L1 pass
//      if(roiThr >= m_trigL1[iChain]){
//        m_h_countL1[iChain]->Fill(roiEta);
//        ATH_MSG_INFO("Passed L1 in FillRateHist()");
//      }
//    }
//
//    //SA
//    //Trig::FeatureContainer fc = m_trigDecTool->features( m_trigHLT[iChain], TrigDefs::alsoDeactivateTEs );
//    //const std::vector< Trig::Feature<xAOD::L2StandAloneMuonContainer> > fL2SAs = fc.get<xAOD::L2StandAloneMuonContainer>( "", TrigDefs::alsoDeactivateTEs );
//    //ATH_MSG_INFO("fL2SAs size = " << fL2SAs.size());
//    //Trig::ExpertMethods* expert = m_trigDecTool -> ExperimentalAndExpertMethods();
//    //expert->enable();
//    //for(auto fL2SA : fL2SAs){
//    //  const HLT::TriggerElement *trigElementSA = fL2SA.te();
//    //  std::vector<HLT::TriggerElement*> TEsuccessors = expert->getNavigation()->getDirectSuccessors(trigElementSA);
//    //  Bool_t isActiveTE = kFALSE;
//    //  for(auto te : TEsuccessors){
//    //    if ( te -> getActiveState() ){
//    //      TString teName = Trig::getTEName( *te );
//    //      if ( (teName.Contains( "L2_mu_SAhyp") && teName.Contains( trigSAName.c_str() ) )|| ( teName.Contains("L2_mu_hypo1" ) ) )
//    //        isActiveTE = kTRUE;
//    //    }
//    //    ATH_MSG_INFO("TEName = " << Trig::getTEName( *te ) << ", getActiveState = " << te -> getActiveState() << ", isActiveTE = " << isActiveTE);
//    //  }
//    //  const xAOD::L2StandAloneMuonContainer* cont = fL2SA.cptr();
//    //  ATH_MSG_INFO("L2SA cont size = " << cont->size());
//    //  for( const auto& l2sa : *cont ) {
//    //    int l2saRoINum  = l2sa->roiNumber();
//    //    double l2saPt  = l2sa->pt();
//    //    double l2saRoIEta  = l2sa->roiEta();
//    //    ATH_MSG_INFO("L2SA: roiNum = " << l2saRoINum << ", Pt = " << l2saPt <<  ", roiEta = " << l2saRoIEta);
//    //    // tmp fill for check
//    //    // pass and not pass
//    //    m_h_countCB[iChain]->Fill(l2saRoIEta);
//    //    // only pass L2MuonSA
//    //    if(isActiveTE) m_h_countSA[iChain]->Fill(l2saRoIEta);
//    //  }
//    //}
//
//
//    //// CB
//    //Trig::FeatureContainer fcL2CB = m_trigDecTool->features( m_trigHLT[iChain], TrigDefs::alsoDeactivateTEs );
//    //const std::vector< Trig::Feature<xAOD::L2CombinedMuonContainer> > fL2CBs = fcL2CB.get<xAOD::L2CombinedMuonContainer>( "", TrigDefs::alsoDeactivateTEs );
//    //ATH_MSG_INFO("fL2CBs size = " << fL2CBs.size());
//    //Trig::ExpertMethods* expertCB = m_trigDecTool -> ExperimentalAndExpertMethods();
//    //expertCB->enable();
//    //for(auto fL2CB : fL2CBs){
//    //  const HLT::TriggerElement *trigElementCB = fL2CB.te();
//    //  std::vector<HLT::TriggerElement*> TEsuccessors = expertCB->getNavigation()->getDirectSuccessors(trigElementCB);
//    //  Bool_t isActiveTE = kFALSE;
//    //  for(auto te : TEsuccessors){
//    //    if ( te -> getActiveState() ){
//    //      TString teName = Trig::getTEName( *te );
//    //      if ( ( teName.Contains( "L2_mucombhyp") && teName.Contains( trigCBName.c_str() ) ) || teName.Contains( "L2_mu_hypo2" ) )
//    //        isActiveTE = kTRUE;
//    //    }
//    //    const xAOD::L2CombinedMuonContainer* cont = fL2CB.cptr();
//    //    for( const auto& l2cb : *cont ) {
//    //      const xAOD::L2StandAloneMuon* L2SAMuon = NULL;
//    //      const ElementLink< xAOD::L2StandAloneMuonContainer >& Link  = l2cb->muSATrackLink();
//    //      if (Link.isValid()) L2SAMuon = *Link;
//    //      if (L2SAMuon == NULL) {
//    //        ATH_MSG_INFO("hello: No track" );   
//    //      } else {
//    //        ATH_MSG_INFO("hello: l2saRoI = " << L2SAMuon->roiNumber() );
//    //      }
//    //      //ElementLinkVector< xAOD::TrackParticleContainer > newContainer1;
//    //      //if (HLT::OK != getFeaturesLinks<xAOD::TrackParticleContainer,xAOD::TrackParticleContainer>(te,newContainer1,"InDetTrigTrackingxAODCnv_Muon_FTF")) {
//    //      //  ATH_MSG_INFO("No elv trkcon roi0" );   
//    //      //}
//    //      //ATH_MSG_INFO("V1 " << track1EL.dataID() << " "  << track1EL.index() );
//    //      //track1EL = remap_container(track1EL,newContainer1);
//    //      //cout << "hello2" << endl;
//
//    //      //ATH_MSG_INFO("Just check track links... Track 1 pT " << (*track1EL)->pt() << " eta: " << (*track1EL)->eta() << " phi: " << (*track1EL)->phi() );
//    //      //cout << "hello3" << endl;
//    //    }
//    //      //cout << "hello4" << endl;
//
//    //    ATH_MSG_INFO("TEName = " << Trig::getTEName( *te ) << ", getActiveState = " << te -> getActiveState() << ", isActiveTE = " << isActiveTE);
//    //  }
//    //  const xAOD::L2CombinedMuonContainer* cont = fL2CB.cptr();
//    //  ATH_MSG_INFO("L2CB cont size = " << cont->size());
//    //  for( const auto& l2cb : *cont ) {
//    //    const double l2cbPt  = l2cb->pt();
//    //    const double l2cbEta  = l2cb->eta();
//    //    const double l2cbPhi  = l2cb->phi();
//    //    //const double l2cbdR = m_utils.deltaR( muEta, muPhi, l2cbEta, l2cbPhi);
//    //    if (!l2cb->muSATrack()) {
//    //      ATH_MSG_INFO("[No muSATrack] l2cbPt = " << l2cbPt <<  ", l2cbEta = " << l2cbEta << ", l2cbPhi = " << l2cbPhi);
//    //    } else {
//    //      ATH_MSG_INFO("[muSATrack] l2cbPt = " << l2cbPt <<  ", l2cbEta = " << l2cbEta << ", l2cbPhi = " << l2cbPhi);
//    //      ATH_MSG_INFO("[muSATrack] l2saPt = " << l2cb->muSATrack()->pt() <<  ", l2saEta = " << l2cb->muSATrack()->eta() << ", l2saPhi = " << l2cb->muSATrack()->phi());
//    //    }
//    //    //ATH_MSG_INFO("L2CB: roiNum = " << l2saRoINum << ", Pt = " << l2saPt <<  ", roiEta = " << l2saRoIEta);
//    //    //if(isActiveTE) m_h_countCB[iChain]->Fill(l2CBRoIEta);
//    //    //bool dRpass = ( 0.02 > l2cbdR );
//    //    //ATH_MSG_INFO("matchCB, dR: " << l2cbdR);
//    //    //if ( (isActiveTE) && dRpass ){
//    //    //  isPassedCB = 1;
//    //    //  goto RoIMatchingAndCBPassed;
//    //    //}
//    //  }
//    //}
//
//    //// offline
//    //double tmp_offlineMatchedRoiEta = -9999.;
//    //double tmp_offlinePtCutMatchedRoiEta = -9999.;
//    //xAOD::MuonContainer::const_iterator mu_itr = muons->begin();
//    //xAOD::MuonContainer::const_iterator mu_end  = muons->end();
//    //for ( ; mu_itr!=mu_end; ++mu_itr ) {
//    //  const double muPt     = (*mu_itr)->pt();
//    //  ATH_MSG_INFO("offline muon loop, muPt: " << muPt/1000. << ", chain threshold: " << m_trigThreshold[iChain]);
//    //  // off
//    //  tmp_offlineMatchedRoiEta = offlineMatching((*mu_itr), rois, m_trigL1[iChain], m_trigHLT[iChain]);
//    //  if (tmp_offlineMatchedRoiEta > -99) {
//    //    m_h_countOff[iChain] -> Fill(tmp_offlineMatchedRoiEta);
//    //    // off pt cut
//    //    if ( muPt/1000. >= m_trigThreshold[iChain] ) {
//    //      ATH_MSG_INFO("offline pT >= chain threshold");
//    //      m_h_countOffPtCut[iChain] -> Fill(tmp_offlineMatchedRoiEta);
//    //    }
//    //  }
//    //}
//
//  } //end of loop for each chain
//
//
//  return StatusCode::SUCCESS;
//}


//double CalcEffAlg :: offlineMatching ( const xAOD::Muon *muon, const xAOD::MuonRoIContainer *rois, const TagAndProbe::L1Item trigL1, const string trigHLT){
//  ATH_MSG_INFO("offlineMatching()");
//  ATH_MSG_INFO("trigL1 = " << trigL1 << ", trigHLT = " << trigHLT);
//
//  int matchedRoINumber = -1;
//  double matchedRoIEta = -9999.;
//
//  //L1
//  ATH_MSG_INFO("L1 matching in offlineMatching()");
//  int isPassedL1 = -1;
//  const double muPt   = muon->pt();
//  const double muEta  = muon->eta();
//  const double muPhi  = muon->phi();
//  const xAOD::TrackParticle* mutrk = muon->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
//  pair< double, double > extEtaAndPhi;
//  //ATH_MSG_INFO("mutrk1" << extEtaAndPhi.first << ", " << extEtaAndPhi.second);
//  if (mutrk) extEtaAndPhi = m_ext.extTrack( mutrk );
//  //ATH_MSG_INFO("mutrk2" << extEtaAndPhi.first << ", " << extEtaAndPhi.second);
//  const double muExtEta = ( mutrk )? extEtaAndPhi.first:muEta;
//  const double muExtPhi = ( mutrk )? extEtaAndPhi.second:muPhi;
//  //ATH_MSG_INFO("mutrk3" << extEtaAndPhi.first << ", " << extEtaAndPhi.second);
//
//  xAOD::MuonRoIContainer::const_iterator roi_itr = rois->begin();
//  xAOD::MuonRoIContainer::const_iterator roi_end = rois->end();
//  double reqdR = ( muPt < 1000.) ? -0.00001*muPt + 0.18 : 0.08;
//  for(; roi_itr!=roi_end; ++roi_itr){
//    int roiThr       = (*roi_itr)->getThrNumber();
//    double roiEta    = (*roi_itr)->eta();
//    double roiPhi    = (*roi_itr)->phi();
//    const int roiNum = (*roi_itr)->getRoI();
//    const int roiPt  = (*roi_itr)->thrValue();
//    ATH_MSG_INFO("L1: roiNum = " << roiNum
//        << ", roiThr = " << roiThr
//        << ", roiPt = " << roiPt
//        << ", roiEta = " << roiEta);
//    // Check dR
//    const double roidRof  = m_utils.deltaR( muEta, muPhi, roiEta, roiPhi);
//    const double roidRex  = m_utils.deltaR( muExtEta, muExtPhi, roiEta, roiPhi);
//    bool isdR = ( m_useExt )? ( reqdR > roidRex ):( reqdR > roidRof );
//    ATH_MSG_INFO("isdR = " << isdR << ", reqdR = " << reqdR << ", roidRex = " << roidRex << ", roidRof = " << roidRof);
//    // Check L1 pass
//    if( (roiThr >= trigL1) && isdR ){
//      matchedRoINumber = roiNum;
//      reqdR = ( m_useExt )? ( reqdR > roidRex ):( reqdR > roidRof );
//      matchedRoIEta = roiEta;
//      isPassedL1 = 1;
//      ATH_MSG_INFO("Matched in L1");
//    } else {
//      continue;
//    }
//  } // roi loop
//
//  ATH_MSG_INFO("isPassedL1:" << isPassedL1 << ", L1 roiNum: " << matchedRoINumber);
//
//  if ( isPassedL1 < 0 ){
//    return -9999;
//  }
//
//  //SA
//  //ATH_MSG_INFO("SA matching in offlineMatching()");
//  //int isPassedSA = -1;
//  //int l2saRoINum = -1;
//  //std::string trigSAName = "";
//  //getSATEName( trigHLT, trigSAName );
//  //Trig::FeatureContainer fcL2SA = m_trigDecTool->features( trigHLT, TrigDefs::alsoDeactivateTEs );
//  //const std::vector< Trig::Feature<xAOD::L2StandAloneMuonContainer> > fL2SAs = fcL2SA.get<xAOD::L2StandAloneMuonContainer>( "", TrigDefs::alsoDeactivateTEs );
//  //ATH_MSG_INFO("fL2SAs size = " << fL2SAs.size());
//  //Trig::ExpertMethods* expert = m_trigDecTool -> ExperimentalAndExpertMethods();
//  //expert->enable();
//  //for(auto fL2SA : fL2SAs){
//  //  const HLT::TriggerElement *trigElementSA = fL2SA.te();
//  //  std::vector<HLT::TriggerElement*> TEsuccessors = expert->getNavigation()->getDirectSuccessors(trigElementSA);
//  //  Bool_t isActiveTE = kFALSE;
//  //  for(auto te : TEsuccessors){
//  //    if ( te -> getActiveState() ){
//  //      TString teName = Trig::getTEName( *te );
//  //      if ( (teName.Contains( "L2_mu_SAhyp") && teName.Contains( trigSAName.c_str() ) )|| ( teName.Contains("L2_mu_hypo1" ) ) )
//  //        isActiveTE = kTRUE;
//  //    }
//  //    ATH_MSG_INFO("TEName = " << Trig::getTEName( *te ) << ", getActiveState = " << te -> getActiveState() << ", isActiveTE = " << isActiveTE);
//  //  }
//  //  const xAOD::L2StandAloneMuonContainer* cont = fL2SA.cptr();
//  //  ATH_MSG_INFO("L2SA cont size = " << cont->size());
//  //  for( const auto& l2sa : *cont ) {
//  //    l2saRoINum  = l2sa->roiNumber();
//  //    double l2saPt  = l2sa->pt();
//  //    double l2saRoIEta  = l2sa->roiEta();
//  //    if ( l2saRoINum == matchedRoINumber && isActiveTE ){
//  //      isPassedSA = 1;
//  //      goto RoIMatchingAndSAPassed;
//  //    }
//  //  }
//  //}
////RoIMatchingAndSAPassed:
//
//  //ATH_MSG_INFO("isPassedSA:" << isPassedSA << ", SA roiNum: " << l2saRoINum);
//
//  //if ( isPassedSA < 0 ){
//  //  return -9999;
//  //}
//
//  //// CB
//  //ATH_MSG_INFO("CB matching in offlineMatching()");
//  //int isPassedCB = -1;
//  //int l2cbRoINum = -1;
//  //std::string trigCBName = "";
//  //getCBTEName( trigHLT, trigCBName );
//  //Trig::FeatureContainer fcL2CB = m_trigDecTool->features( trigHLT, TrigDefs::alsoDeactivateTEs );
//  //const std::vector< Trig::Feature<xAOD::L2CombinedMuonContainer> > fL2CBs = fcL2CB.get<xAOD::L2CombinedMuonContainer>( "", TrigDefs::alsoDeactivateTEs );
//  //ATH_MSG_INFO("fL2CBs size = " << fL2CBs.size());
//  //Trig::ExpertMethods* expertCB = m_trigDecTool -> ExperimentalAndExpertMethods();
//  //expertCB->enable();
//  //for(auto& fL2CB : fL2CBs){
//  //  const HLT::TriggerElement *trigElementCB = fL2CB.te();
//  //  std::vector<HLT::TriggerElement*> TEsuccessors = expertCB->getNavigation()->getDirectSuccessors(trigElementCB);
//  //  Bool_t isActiveTE = kFALSE;
//  //  for(auto te : TEsuccessors){
//  //    if ( te -> getActiveState() ){
//  //      TString teName = Trig::getTEName( *te );
//  //      if ( ( teName.Contains( "L2_mucombhyp") && teName.Contains( trigCBName.c_str() ) ) || teName.Contains( "L2_mu_hypo2" ) )
//  //        isActiveTE = kTRUE;
//  //    }
//  //    ATH_MSG_INFO("TEName = " << Trig::getTEName( *te ) << ", getActiveState = " << te -> getActiveState() << ", isActiveTE = " << isActiveTE);
//  //  }
//  //  const xAOD::L2CombinedMuonContainer* cont = fL2CB.cptr();
//  //  ATH_MSG_INFO("L2CB cont size = " << cont->size());
//  //  for( const auto& l2cb : *cont ) {
//  //    const double l2cbEta  = l2cb->eta();
//  //    const double l2cbPhi  = l2cb->phi();
//  //    const double l2cbdR = m_utils.deltaR( muEta, muPhi, l2cbEta, l2cbPhi);
//  //    bool dRpass = ( 0.02 > l2cbdR );
//  //    //ATH_MSG_INFO("matchCB, dR: " << l2cbdR);
//  //    if ( (isActiveTE) && dRpass ){
//  //      isPassedCB = 1;
//  //      goto RoIMatchingAndCBPassed;
//  //    }
//  //  }
//  //}
////RoIMatchingAndCBPassed:
//
//  //ATH_MSG_INFO("isPassedCB:" << isPassedCB );
//
//  //if ( isPassedCB < 0 ){
//  //  return -9999;
//  //}
//
//
//  // EF
//  //ATH_MSG_INFO("EF matching in offlineMatching()");
//  //int isPassedEF = -1;
//  //Trig::FeatureContainer fcEF = m_trigDecTool->features( trigHLT, TrigDefs::alsoDeactivateTEs );
//  //const std::vector< Trig::Feature<xAOD::MuonContainer> > fEFs = fcEF.get<xAOD::MuonContainer>( "", TrigDefs::alsoDeactivateTEs );
//
//  //double efPt     = -99999;
//  //double efEta    = -99999;
//  //double efPhi    = -99999;
//  //double efCharge = -99999;
//  //double efdRmu   = 0.02;
//  //for ( auto& fEF : fEFs ) {
//  //  const HLT::TriggerElement* efTE = ( fEF.te() );
//  //  const xAOD::MuonContainer* cont = fEF.cptr();
//  //  for( const auto& ef : *cont ) {
//  //    if( 0.02 < m_utils.deltaR( muEta, muPhi, ef->eta(), ef->phi() ) ) continue;
//  //    if( !efTE->getActiveState() ) continue;
//  //    efPt      = ef->pt();
//  //    efEta     = ef->eta();
//  //    efPhi     = ef->phi();
//  //    efCharge  = ef->charge();
//  //    efdRmu    = m_utils.deltaR( muEta, muPhi, efEta, efPhi );
//  //  }
//  //}
//  //bool pass = ( m_utils.muonEFThreshold( efEta, efPt*0.001, trigHLT ) );
//  ////if( m_message>1 ) cout << "EF : " << muPt << ", " << efPt << " ==> " << chain << ": " << pass << endl;
//  //if( pass ) {
//  //  isPassedEF = 1;
//  //}
//
//  ATH_MSG_INFO("isPassedEF:" << isPassedEF);
//
//  if ( isPassedEF < 0 ){
//    return -9999;
//  }
//
//  return matchedRoIEta;
//}


//Bool_t CalcEffAlg::getSATEName( const std::string& mesHLT, std::string& teName )
//{
//  ///This function is used in "int TagAndProbe::addMesChain ".
//  ///mesHLT and teName is parameters.
//  TString hlt = mesHLT.c_str();
//  TObjArray* toa = hlt . Tokenize( "_" );
//  Int_t thrValue = 0;
//  Bool_t isBarrelOnly = kFALSE;
//  for( Int_t i = 0; i < toa -> GetEntries(); i++ ){
//    TString tsToken = (static_cast<TObjString*>( toa -> At(i) ) ) -> String();
//    if ( tsToken . Contains( "0eta105" ) ){
//      isBarrelOnly = kTRUE;
//    }
//    if ( !( tsToken.Contains( "mu" ) || tsToken.Contains( "MU" ) ) ) continue;
//    tsToken = tsToken.ReplaceAll( "noL1", "" );
//    tsToken = tsToken.ReplaceAll( "L1", "" );
//    tsToken = tsToken.ReplaceAll( "3mu", "" );
//    tsToken = tsToken.ReplaceAll( "2mu", "" );
//    tsToken = tsToken.ReplaceAll( "mu", "" );
//    tsToken = tsToken.ReplaceAll( "MU", "" );
//    if ( !tsToken . IsDec() )
//      continue;
//    thrValue = tsToken . Atoi();
//  }
//  if ( thrValue == 0 )
//    return 1;
//  if ( thrValue == 4 || thrValue == 2 || isBarrelOnly ){
//  } else {
//    thrValue = 6;
//  }
//  teName = Form( "%dGeV%s_v15a", thrValue, (isBarrelOnly)?("_barrelOnly"):("") );
//  ATH_MSG_INFO("CalcEffAlg: Derived TESAName=" << teName);
//  return 0;
//}

//Bool_t CalcEffAlg::getCBTEName( const std::string& mesHLT, std::string& teName )
//{
//  TString hlt = mesHLT.c_str();
//  TObjArray* toa = hlt . Tokenize( "_" );
//  Int_t thrValue = 0;
//  for( Int_t i = 0; i < toa -> GetEntries(); i++ ){
//    TString tsToken = (static_cast<TObjString*>( toa -> At(i) ) ) -> String();
//    if ( !( tsToken.Contains( "mu" ) || tsToken.Contains( "MU" ) ) ) continue;
//    tsToken = tsToken.ReplaceAll( "noL1", "" );
//    tsToken = tsToken.ReplaceAll( "L1", "" );
//    tsToken = tsToken.ReplaceAll( "3mu", "" );
//    tsToken = tsToken.ReplaceAll( "2mu", "" );
//    tsToken = tsToken.ReplaceAll( "mu", "" );
//    tsToken = tsToken.ReplaceAll( "MU", "" );
//    if ( !tsToken . IsDec() )
//      continue;
//    thrValue = tsToken . Atoi();
//  }
//  if ( thrValue == 0 )
//    return 1;
//  if ( thrValue >= 24 )
//    thrValue = 22;
//  teName = Form( "_mu%d_", thrValue );
//  ATH_MSG_INFO("CalcEffAlg: Derived TECBName=" << teName);
//  return 0;
//}
double CalcEffAlg::deltaR( const double eta1, const double phi1, const double eta2, const double phi2 ) {
  double deltaEta = fabs( eta1 - eta2 );
  double deltaPhi = acos(cos(phi1-phi2));//*acos(cos(phi1-phi2));
  return sqrt( deltaEta*deltaEta + deltaPhi*deltaPhi );
}

double CalcEffAlg::pTresidual( double offline_pt, double trig_pt){
  const double ZERO_LIMIT = 1.e-10;
  if ( abs(trig_pt) < ZERO_LIMIT ) {
    ATH_MSG_DEBUG("pTresidual: trig_pt < ZERO_LIMIT, " << offline_pt << "/" << trig_pt);
    return -999999;
    } else {
      ATH_MSG_DEBUG("pTresidual: trig_pt > ZERO_LIMIT, " << ( 1. - (offline_pt) / abs(trig_pt)) << ", " << offline_pt << "/" << trig_pt);
    }
  return ( 1. - (offline_pt) / abs(trig_pt));
}

//08:26:29 MuFastSteering_MuonIOmode.TrigL2MuonS...29  0     DEBUG ... Superpoint chamber/s_address/count/R/Z/Alin/Blin/Phim/Xor/Yor/Chi2/PChi2=
//                                                                                2/3/4/10529.5/-9796.08/-0.990291/0.673044/-1     .9635/10449.5/-9717.49/740.333/0

int CalcEffAlg::NumberOfSP(const xAOD::L2StandAloneMuon* l2sa){
  int number = 0;
  if (abs(l2sa->superPointR(0)) > 0 && l2sa->superPointZ(0) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(1)) > 0 && l2sa->superPointZ(1) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(2)) > 0 && l2sa->superPointZ(2) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(3)) > 0 && l2sa->superPointZ(3) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(4)) > 0 && l2sa->superPointZ(4) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(5)) > 0 && l2sa->superPointZ(5) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(6)) > 0 && l2sa->superPointZ(6) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(7)) > 0 && l2sa->superPointZ(7) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(8)) > 0 && l2sa->superPointZ(8) > -99990.) {
    number += 1;
  }
  if (abs(l2sa->superPointR(9)) > 0 && l2sa->superPointZ(9) > -99990.) {
    number += 1;
  }
  return number;
}

