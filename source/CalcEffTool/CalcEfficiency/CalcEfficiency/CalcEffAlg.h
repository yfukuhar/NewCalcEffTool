#ifndef CALCEFFALG_H // Include guard
#define CALCEFFALG_H

// Base
#include "AthenaBaseComps/AthAlgorithm.h"

class ITHistSvc;
class TH1D;
class TH2D;

// ASG Tools
#include "AsgTools/AsgToolsConf.h"
#include "AsgTools/AsgMetadataTool.h"
#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODTrigMuon/L2StandAloneMuon.h"
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigMuon/L2CombinedMuon.h"
#include "xAODTrigMuon/L2CombinedMuonContainer.h"
#include "xAODTrigger/MuonRoIContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TFile.h"

// Official Tools
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigConfInterfaces/ITrigConfigSvc.h"
//#include "GoodRunsLists/IGoodRunsListSelectionTool.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"

#include "xAODTrigger/TrigCompositeContainer.h"
#include "xAODTrigger/TrigCompositeAuxContainer.h"
#include "xAODTrigger/versions/TrigCompositeAuxContainer_v1.h"
#include "xAODTrigger/MuonRoIContainer.h"


#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/LockedHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigger/versions/TrigComposite_v1.h"
#include "TrigConfData/HLTMenu.h"
#include "TrigConfData/L1Menu.h"
#include "TrigConfData/DataStructure.h"
#include "TrkExInterfaces/IExtrapolator.h"
            
#include "CalcEfficiency/MuonExtUtils.h"

using namespace TrigCompositeUtils;


// ROOT

// My class

// Number of target trigger-chains for trigger rate
const int NChain = 4;

// foaward
class GoodRunsListSelectionTool;
namespace CP {
  class MuonCalibrationAndSmearingTool; 
}
namespace TrigConf {
  class xAODConfigTool;
  class ITrigConfigTool;
}
namespace Trk { 
  class IExtrapolator; 
  class IVertexFitter; 
}


class CalcEffAlg:public AthAlgorithm
{

  public:
    CalcEffAlg(const std::string& name, ISvcLocator* pSvcLocator); // Constructor
    enum L1Item{ NOTHING, L1_MU4, L1_MU6, L1_MU10, L1_MU11, L1_MU15, L1_MU20 };
    enum TapType{ L1, L2, EF, ALL };

    StatusCode initialize();
    StatusCode finalize();
    StatusCode execute();
    StatusCode checkFtk();
    StatusCode FillRateHist(const xAOD::MuonContainer* muons, const xAOD::MuonRoIContainer* rois );
    Bool_t getSATEName( const std::string& mesHLT, std::string& teName );
    Bool_t getCBTEName( const std::string& mesHLT, std::string& teName );
    double deltaR( const double eta1, const double phi1, const double eta2, const double phi2 );
    double pTresidual( const double pToffline, const double pTtrigger );
    int NumberOfSP(const xAOD::L2StandAloneMuon* l2sa);
    

  private:
    std::string m_message;
    std::string m_etname;
    std::string m_dataType;
    bool m_isFirstEvent; //!

    //SG::ReadHandleKey<xAOD::L2StandAloneMuonContainer> m_L2MuonSAContainerKey;


    // GRL tool
    ToolHandle<IGoodRunsListSelectionTool> m_grlTool;
    bool m_useGRL; //!

    // The decision tool

    //SG::ReadHandleKey<TrigConf::HLTMenu> m_HLTMenuKey{ this, "HLTTriggerMenu", "DetectorStore+HLTTriggerMenu", "HLT Menu" };
    //SG::ReadHandleKey<TrigConf::L1Menu> m_L1MenuKey{ this, "L1TriggerMenu", "DetectorStore+L1TriggerMenu", "L1 Menu" };

    // Extrapolator Class
    ToolHandle<Trk::IExtrapolator> m_extrapolator;

    MuonExtUtils m_ext; //!

    std::string m_tapmethod; //!
    bool m_useExt; //!

    // EventTree Class
    TFile* m_file;

    //xAOD::L2StandAloneMuonContainer* m_l2sas;

    // Rate Trigger
    ITHistSvc *m_thistSvc;
    //TH1F *m_h_rateL1[NChain];
    //TH1F *m_h_rateSA[NChain];
    //TH1F *m_h_rateOff[NChain];
    //TH1F *m_h_rateOffPtCut[NChain];
    std::vector< TH1D* > m_h_countEvent;
    std::vector< TH1F* > m_h_countL1;
    std::vector< TH1F* > m_h_countSA;
    std::vector< TH1F* > m_h_countCB;
    std::vector< TH1F* > m_h_countEF;
    std::vector< TH1F* > m_h_countOff;
    std::vector< TH1F* > m_h_countOffPtCut;

    std::vector< TH1F* > m_h_extFlag;
    std::vector< TH1F* > m_h_ptFtk;
    std::vector< TH1F* > m_h_etaFtk;
    std::vector< TH1F* > m_h_phiFtk; 

    TH1F* m_h_dimumass;
    TH1F* m_h_l2cbio_dimuMass;
    TH1F* m_h_di_ext_deltaR;
    TH1F* m_h_di_IP_deltaR;
    TH2F* m_h_di_ext_IP_deltaR;
    TH2F* m_h_di_dR_pt;

    TH2F* m_h_pt_alpha;
    TH2F* m_h_pt_beta;
    TH2F* m_h_pt_alpha1;
    TH2F* m_h_pt_beta1;

    TH1F* m_h_pt_muL1;
    TH1F* m_h_pt_muSA;
    TH1F* m_h_pt_muCB;
    TH1F* m_h_pt_muSAIO;
    TH1F* m_h_pt_muCBIO;
    TH1F* m_h_pt_muL1_1;
    TH1F* m_h_pt_muSA_1;
    TH1F* m_h_pt_muCB_1;
    TH1F* m_h_pt_muSAIO_1;
    TH1F* m_h_pt_muCBIO_1;

    TH2F* m_h_pt_dR_L1_0;
    TH2F* m_h_pt_dR_SA_0;
    TH2F* m_h_pt_dR_CB_0;
    TH2F* m_h_pt_dR_SAIO_0;
    TH2F* m_h_pt_dR_CBIO_0;

    TH2F* m_h_pt_dR_L1_1;
    TH2F* m_h_pt_dR_SA_1;
    TH2F* m_h_pt_dR_CB_1;
    TH2F* m_h_pt_dR_SAIO_1;
    TH2F* m_h_pt_dR_CBIO_1;

    TH2F* m_h_pt_dR_L1_2;
    TH2F* m_h_pt_dR_SA_2;
    TH2F* m_h_pt_dR_CB_2;
    TH2F* m_h_pt_dR_SAIO_2;
    TH2F* m_h_pt_dR_CBIO_2;

    TH2F* m_h_pt_dR_L1_3;
    TH2F* m_h_pt_dR_SA_3;
    TH2F* m_h_pt_dR_CB_3;
    TH2F* m_h_pt_dR_SAIO_3;
    TH2F* m_h_pt_dR_CBIO_3;

    TH2F* m_h_pt_dR_L1_4;
    TH2F* m_h_pt_dR_SA_4;
    TH2F* m_h_pt_dR_CB_4;
    TH2F* m_h_pt_dR_SAIO_4;
    TH2F* m_h_pt_dR_CBIO_4;


    TH2F* m_h_ftf_size;
    TH2F* m_h_ftf_size1;
    TH2F* m_h_ftf_size2;

    TH2F* m_h_pt_res_muL1;
    TH2F* m_h_pt_res_muSA;
    TH2F* m_h_pt_res_muCB;
    TH2F* m_h_pt_res_muSAIO;
    TH2F* m_h_pt_res_muCBIO;

    TH2F* m_h_pt_res_muL1_1;
    TH2F* m_h_pt_res_muSA_1;
    TH2F* m_h_pt_res_muCB_1;
    TH2F* m_h_pt_res_muSAIO_1;
    TH2F* m_h_pt_res_muCBIO_1;

    TH2F* m_h_pt_res_muL1_2;
    TH2F* m_h_pt_res_muSA_2;
    TH2F* m_h_pt_res_muCB_2;
    TH2F* m_h_pt_res_muSAIO_2;
    TH2F* m_h_pt_res_muCBIO_2;

    TH2F* m_h_pt_res_muL1_3;
    TH2F* m_h_pt_res_muSA_3;
    TH2F* m_h_pt_res_muCB_3;
    TH2F* m_h_pt_res_muSAIO_3;
    TH2F* m_h_pt_res_muCBIO_3;

    TH2F* m_h_pt_resSeg_muL1;
    TH2F* m_h_pt_resSeg_muSA;
    TH2F* m_h_pt_resSeg_muCB;
    TH2F* m_h_pt_resSeg_muSAIO;
    TH2F* m_h_pt_resSeg_muCBIO;

    TH2F* m_h_pt_resSeg_muL1_1;
    TH2F* m_h_pt_resSeg_muSA_1;
    TH2F* m_h_pt_resSeg_muCB_1;
    TH2F* m_h_pt_resSeg_muSAIO_1;
    TH2F* m_h_pt_resSeg_muCBIO_1;

    TH2F* m_h_pt_resSeg_muL1_2;
    TH2F* m_h_pt_resSeg_muSA_2;
    TH2F* m_h_pt_resSeg_muCB_2;
    TH2F* m_h_pt_resSeg_muSAIO_2;
    TH2F* m_h_pt_resSeg_muCBIO_2;

    TH2F* m_h_pt_numberOfSPs_muL1;
    TH2F* m_h_pt_numberOfSPs_muSA;
    TH2F* m_h_pt_numberOfSPs_muCB;
    TH2F* m_h_pt_numberOfSPs_muSAIO;
    TH2F* m_h_pt_numberOfSPs_muCBIO;

    TH2F* m_h_pt_numberOfSPs_muL1_1;
    TH2F* m_h_pt_numberOfSPs_muSA_1;
    TH2F* m_h_pt_numberOfSPs_muCB_1;
    TH2F* m_h_pt_numberOfSPs_muSAIO_1;
    TH2F* m_h_pt_numberOfSPs_muCBIO_1;

    TH2F* m_h_pt_numberOfSPs_muL1_2;
    TH2F* m_h_pt_numberOfSPs_muSA_2;
    TH2F* m_h_pt_numberOfSPs_muCB_2;
    TH2F* m_h_pt_numberOfSPs_muSAIO_2;
    TH2F* m_h_pt_numberOfSPs_muCBIO_2;

    TH2F* m_h_pt_numberOfFTFs;
    TH2F* m_h_pt_numberOfFTFs1;
    TH2F* m_h_pt_numberOfFTFs2;

    TH1F* m_h_dR_2muL1;
    TH1F* m_h_dR_2muSA;
    TH1F* m_h_dR_2muCB;
    TH1F* m_h_dR_2muSAIO;
    TH1F* m_h_dR_2muCBIO;
    TH1F* m_h_dR_2mutmp1;
    TH1F* m_h_dR_2mutmp2;
    TH1F* m_h_dR_2mutmp3;
    TH1F* m_h_dR_2mutmp4;

    TH1F* m_h_dR_2muL1_sep;
    TH1F* m_h_dR_2muSA_sep;
    TH1F* m_h_dR_2muCB_sep;
    TH1F* m_h_dR_2muSAIO_sep;
    TH1F* m_h_dR_2muCBIO_sep;

    TH1F* m_h_dR_2muL1_sep1;
    TH1F* m_h_dR_2muSA_sep1;
    TH1F* m_h_dR_2muCB_sep1;
    TH1F* m_h_dR_2muSAIO_sep1;
    TH1F* m_h_dR_2muCBIO_sep1;

    TH1F* m_h_dR_2muL1_sep2;
    TH1F* m_h_dR_2muSA_sep2;
    TH1F* m_h_dR_2muCB_sep2;
    TH1F* m_h_dR_2muSAIO_sep2;
    TH1F* m_h_dR_2muCBIO_sep2;

    TH1F* m_h_dR_2muL1_sep3;
    TH1F* m_h_dR_2muSA_sep3;
    TH1F* m_h_dR_2muCB_sep3;
    TH1F* m_h_dR_2muSAIO_sep3;
    TH1F* m_h_dR_2muCBIO_sep3;

    TH1F* m_h_dR_2muL1_sep4;
    TH1F* m_h_dR_2muSA_sep4;
    TH1F* m_h_dR_2muCB_sep4;
    TH1F* m_h_dR_2muSAIO_sep4;
    TH1F* m_h_dR_2muCBIO_sep4;

    TH1F* m_h_dR_2muL1_sep5;
    TH1F* m_h_dR_2muSA_sep5;
    TH1F* m_h_dR_2muCB_sep5;
    TH1F* m_h_dR_2muSAIO_sep5;
    TH1F* m_h_dR_2muCBIO_sep5;

    TH1F* m_h_dR_2muL1_sep6;
    TH1F* m_h_dR_2muSA_sep6;
    TH1F* m_h_dR_2muCB_sep6;
    TH1F* m_h_dR_2muSAIO_sep6;
    TH1F* m_h_dR_2muCBIO_sep6;


    TTree* m_tree;
    int m_nMuons;
    int m_nFtks;
    int m_eventNumber;
    int m_runNumber;
    int m_lumiBlock;
    double m_averageInteractionsPerCrossing;
    std::vector<int> *m_muon_author;
    std::vector<int> *m_muon_type;
    std::vector<double> *m_muon_phi;
    std::vector<double> *m_muon_eta;
    std::vector<double> *m_muon_extphi;
    std::vector<double> *m_muon_exteta;
    std::vector<double> *m_muon_charge;
    std::vector<double> *m_muon_pt;

    std::vector<int> *m_ftf_size;
    std::vector<int> *m_ftf_size1;
    std::vector<int> *m_ftf_size2;

    std::vector<double> *m_l1_pt;
    std::vector<double> *m_l1_eta;
    std::vector<double> *m_l1_phi;

    std::vector<double> *m_l2sa_pt;
    std::vector<double> *m_l2sa_eta;
    std::vector<double> *m_l2sa_phi;

    std::vector<double> *m_l2cb_pt;
    std::vector<double> *m_l2cb_eta;
    std::vector<double> *m_l2cb_phi;

    std::vector<double> *m_l2saio_pt;
    std::vector<double> *m_l2saio_eta;
    std::vector<double> *m_l2saio_phi;

    std::vector<double> *m_l2cbio_pt;
    std::vector<double> *m_l2cbio_eta;
    std::vector<double> *m_l2cbio_phi;

    std::vector<int> *m_l1_numberOfSPs0;
    std::vector<int> *m_l2sa_numberOfSPs0;
    std::vector<int> *m_l2cb_numberOfSPs0;
    std::vector<int> *m_l2saio_numberOfSPs0;
    std::vector<int> *m_l2cbio_numberOfSPs0;

    std::vector<int> *m_l1_numberOfSPs1;
    std::vector<int> *m_l2sa_numberOfSPs1;
    std::vector<int> *m_l2cb_numberOfSPs1;
    std::vector<int> *m_l2saio_numberOfSPs1;
    std::vector<int> *m_l2cbio_numberOfSPs1;

    //std::vector<int> *m_l2saio_numberOfFTFs1;
    //std::vector<int> *m_l2saio_numberOfFTFs1;

    std::vector<double> *m_l1_resSeg_0;
    std::vector<double> *m_l2sa_resSeg_0;
    std::vector<double> *m_l2cb_resSeg_0;
    std::vector<double> *m_l2saio_resSeg_0;
    std::vector<double> *m_l2cbio_resSeg_0;

    std::vector<double> *m_l1_resSeg_1;
    std::vector<double> *m_l2sa_resSeg_1;
    std::vector<double> *m_l2cb_resSeg_1;
    std::vector<double> *m_l2saio_resSeg_1;
    std::vector<double> *m_l2cbio_resSeg_1;

    std::vector<double> *m_l1_resSeg_2;
    std::vector<double> *m_l2sa_resSeg_2;
    std::vector<double> *m_l2cb_resSeg_2;
    std::vector<double> *m_l2saio_resSeg_2;
    std::vector<double> *m_l2cbio_resSeg_2;


    std::vector<int> *m_l1_pass;
    std::vector<int> *m_l2sa_pass;
    std::vector<int> *m_l2cb_pass;
    std::vector<int> *m_l2saio_pass;
    std::vector<int> *m_l2cbio_pass;

    std::vector<double> *m_ftk_pt;
    std::vector<double> *m_ftk_eta;
    std::vector<double> *m_ftk_phi;
    std::vector<double> *m_ftk_charge;
    std::vector<double> *m_ftk_d0;
    std::vector<double> *m_ftk_z0;

    std::vector< std::string > m_trigL1; //!
    std::vector< std::string > m_trigEvent; //!
    std::vector< std::string > m_trigHLT; //!
    std::vector< double > m_trigThreshold; //!
};

#endif // CALCALG_H
